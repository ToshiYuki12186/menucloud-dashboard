/*!
 * 
 * Angle - Bootstrap Admin App + AngularJS
 * 
 * Noocleus Angle Theme Seed: 0.1
 * Author: v.nikopolidi@gmaill.com * 
 * License: https://wrapbootstrap.com/help/licenses
 * 
 */

// APP START
// ----------------------------------- 

'use strict';
var apiBaseUrl = '';
var prefix = "/api/v1";
// gulp:overrideApiBaseUrl 
apiBaseUrl = 'http://demo-api.menucloud.io';
// gulp:endOverrideApiBaseUrl
var apiEndPoint = apiBaseUrl + prefix;
io.sails.url = apiBaseUrl;
(function () {
  angular
    .module('menucloud', [
      // global bower modules
      'ngSails'
      , 'ngAutodisable'
      , 'dialogs.main'
      , 'ui.bootstrap'
      , 'ui.select'
      , 'angular-momentjs'
      // ,'toaster'
      // ,'ui.ace'
      /*
       // has to get values from this provider and set it manually at app.translate
       ,'dialogs.default-translations'
       */
      // app components
      , 'app.core'
      , 'app.auth'
      , 'app.routes'
      , 'app.factories'
      , 'app.loadingbar'
      , 'app.navsearch'
      , 'app.preloader'
      , 'app.translate'
      , 'app.pages'
      , 'app.directives'
      , 'app.panels'
      , 'app.material'
      , 'app.settings'
      , 'app.sidebar'
      , 'app.utils'
      , 'app.material'
      , 'app.toaster'
      , 'app.elements'
      , 'app.dashboard'
      , 'app.restaurant'
      , 'app.subtopnavbar'
      , 'app.profile'
      // custom modules
      , 'menucloud.services'
      // other
    ]);
})();


(function() {
    angular
        .module('menucloud')
        .config(AppConfig);

    AppConfig.$inject = ['$sailsProvider','dialogsProvider'];
    function AppConfig($sailsProvider,dialogsProvider){
        /*
        ngSails
        */
        // console.log('apiBaseUrl prefix: ', apiBaseUrl, prefix)
        $sailsProvider.url = apiBaseUrl
        $sailsProvider.urlPrefix = prefix
        // console.log("$sailsProvider.url: ", $sailsProvider.url )
        dialogsProvider.useBackdrop('static');
        dialogsProvider.useFontAwesome()
        // dialogsProvider.useEscClose(false);
        // dialogsProvider.useCopy(false);
        dialogsProvider.setSize('sm');
    };

})();
(function() {
    'use strict';

    angular
        .module('app.auth', [
          'ngAutodisable',
          'ngRoute',
          'ngAnimate',
          'ngStorage',
          'ngCookies',
          'pascalprecht.translate',
          'ui.bootstrap',
          'ui.router',
          'oc.lazyLoad',
          'cfp.loadingBar',
          'ngSanitize',
          'ngResource',
          'ui.utils',
          'ngAutocomplete'
        ]);
})();
(function() {
    'use strict';

    angular
        .module('app.colors', []);
})();
(function() {
    'use strict';

    angular
        .module('app.core', [
            'ngRoute',
            'ngAnimate',
            'ngStorage',
            'ngCookies',
            'pascalprecht.translate',
            'ui.bootstrap',
            'ui.router',
            'oc.lazyLoad',
            'cfp.loadingBar',
            'ngSanitize',
            'ngResource',
            'ui.utils',
            'ngAria',
            'ngMessages'
        ]);
})();
(function() {
    'use strict';
    angular
        .module('app.directives', [ ]);
})();
(function() {
    'use strict';

    angular
        .module('app.factories', [
            'ngSails',
            'ngStorage',
            'ngCookies',
            'pascalprecht.translate',
            'cfp.loadingBar',
            'ngSanitize',
            'ngResource'
        ]);
})();
(function() {
    'use strict';

    angular
        .module('app.elements', []);
})();
(function() {
    'use strict';

    angular
        .module('app.loadingbar', []);
})();
(function() {
    'use strict';

    angular
        .module('app.lazyload', []);
})();
(function() {
    'use strict';

    angular
        .module('app.material', [
            'ngMaterial'
          ]);
})();
(function() {
    'use strict';

    angular
        .module('app.navsearch', []);
})();
(function() {
    'use strict';

    angular
      .module('app.pages', []);
})();
(function() {
    'use strict';

    angular
        .module('app.panels', []);
})();
(function() {
    'use strict';

    angular
        .module('app.preloader', []);
})();


(function () {
  'use strict';

  angular
    .module('app.profile', []);
})();
(function() {
    'use strict';

    angular
        .module('app.routes', [
            'app.lazyload'
        ]);
})();
(function() {
    'use strict';

    angular
        .module('app.settings', []);
})();
(function() {
    'use strict';

    angular
        .module('app.sidebar', []);
})();
(function() {
    'use strict';

    angular
        .module('app.subtopnavbar', [
            'ngAutodisable',
            'ngRoute',
            'ngAnimate',
            'ngStorage',
            'ngCookies',
            'pascalprecht.translate',
            'ui.bootstrap',
            'ui.router',
            'oc.lazyLoad',
            'cfp.loadingBar',
            'ngSanitize',
            'ngResource',
            'ui.utils',
            'ngAutocomplete'
        ]);
})();
(function() {
    'use strict';

    angular
        .module('app.toaster', [
            'toaster'
            ,"ngAnimate"
        ]);
})();
(function() {
    'use strict';

    angular
        .module('app.translate', []);
})();
(function() {
    'use strict';

    angular
        .module('app.utils', [
          'app.colors'
          ]);
})();

(function () {
  'use strict';

  angular
    .module('app.auth')
    .controller('AuthLockCtrl', AuthLockCtrl);


  AuthLockCtrl.$inject = ['$rootScope', '$scope', 'AuthService', '$state'];

  function AuthLockCtrl($rootScope, $scope, AuthService, $state) {
    var vm = this;

    activate()

    function activate() {
      // bind here all data from the form
      vm.account = {};
      // place the message if something goes wrong
      vm.errMsg = '';
      // set form data
      setFormData();
      if ($rootScope.user) {
        $rootScope.auth.form.title.name = $rootScope.user.first_name + ' ' + $rootScope.user.last_name;
      }

      function modifyBody(data) {
        var body = {
          email: $rootScope.user.email,
          password: data.password
        };
        return body
      }

      vm.login = function () {
        vm.errMsg = '';
        vm.okMsg = '';

        if (vm.loginForm.$valid) {
          // console.log('vm.account: ', vm.account)
          vm.loading = true
          var body = modifyBody(vm.account)
          console.log('account: ', body);
          AuthService.login(body).then(
            function (payload) {
              vm.loading = false
              $rootScope.$emit('$login', payload)
              $state.go('app.dashboard');
            }
            , function (err) {
              vm.loading = false
              console.log('result: ', arguments)
              if (err) {

                if (err.message) {
                  vm.errMsg = err.message
                } else {
                  if (_.isString(err)) {
                    vm.errMsg = err
                  } else {
                    vm.errMsg = "Unable to Login"
                  }
                }
              } else {
                vm.errMsg = "Server Request Error"
              }
            }
            , function (status) {
              vm.loading = false
              console.log('status: ', status)
              vm.unverified = body.email
            }
          )
        }
        else {
          // set as dirty if the user click directly to login so we show the validation messages
          /*jshint -W106*/
          vm.loginForm.account_password.$dirty = true;
        }
      }

      function setFormData() {
        $rootScope.auth = {
          form: {
            title: {
              name: '',
              description: '',
              hasIcon: true,
              icon: 'icon-lock-screen'
            }
          },
          popup: {
            showPopup: false,
            title: '',
            receiverEmail: null,
            okUri: 'page.login',
            okText: ''
          }
        };
      }
    }
  }
})();

/**=========================================================
 * Component: access-register.js
 * Demo for register account api
 =========================================================*/

(function () {
  'use strict';

  angular
    .module('app.auth')
    .controller('AuthLoginCtrl', AuthLoginCtrl);


  AuthLoginCtrl.$inject = ['$rootScope', '$scope', 'AuthService', '$state'];

  function AuthLoginCtrl($rootScope, $scope, AuthService, $state) {
    var vm = this;
    // check if user already logged in
    // console.log('AuthService.isAuthenticated: ', AuthService.isAuthenticated())
    if (AuthService.isAuthenticated()) {
      AuthService.me().then(
        function (user) {
          authorize()
        },
        function (err) {
          var msg = 'Session expired'
          var errMsg = msg
          if (err) {
            if (_.isString(err)) {
              errMsg = err
            } else {
              errMsg = err.message || err.msg || errMsg
            }
          }
        }
      )
    } else {
      // not authorized
    }
    activate()
    ////////////////
    function authorize() {
      $state.go('app.dashboard')
    }

    function activate() {
      // bind here all data from the form
      vm.account = {};
      // place the message if something goes wrong
      vm.errMsg = '';

      // set form data
      setFormData();

      function modifyBody(data) {
        var body = {
          email: data.email,
          password: data.password
        };
        return body
      }

      vm.getVerified = function () {
        vm.errMsg = '';
        vm.okMsg = '';
        if (vm.account.email) {
          AuthService.getVerified(vm.account.email)
            .then(
              function (data) {
                vm.unverified = null
                vm.okMsg = data
              }, function (err) {
                vm.errMsg = err
              }
            )
        }
      }

      vm.login = function () {
        vm.errMsg = '';
        vm.okMsg = '';
        vm.unverified = null;

          if(vm.loginForm.$valid) {
            // console.log('vm.account: ', vm.account)
            vm.loading = true
            var body = modifyBody(vm.account)
            AuthService.login(body).then(
              function (payload){
                vm.loading = false
                var user = payload.user
                if(user && user.role == "user"){
                  $rootScope.$emit('$login', payload)
                  $state.go('app.dashboard');
                  // $state.go('page.login')
                }else{
                  vm.errMsg = "You are not allowed to access menucloud dashboard"
                }
              }
              ,function (err){
                vm.loading = false
                console.log('result: ', arguments)
                if(err){
                  
                  if(err.message){
                    vm.errMsg = err.message
                  }else{
                    if(_.isString(err)){
                      vm.errMsg = err
                    }else{
                      vm.errMsg = "Unable to Login"
                    }
                  }
                } else {
                  vm.errMsg = "Server Request Error"
                }
            }
            , function (status) {
              vm.loading = false
              console.log('status: ', status)
              vm.unverified = body.email
            }
          )
        }
        else {
          // set as dirty if the user click directly to login so we show the validation messages
          /*jshint -W106*/
          vm.loginForm.email.$dirty = true;
          vm.loginForm.password.$dirty = true;
        }
      };

      function setFormData() {
        $rootScope.auth = {
          form: {
            title: {
              name: '',
              description: '',
              hasIcon: false,
              icon: ''
            }
          },
          popup: {
            showPopup: false,
            title: '',
            receiverEmail: null,
            okUri: 'page.login',
            okText: ''
          }
        };
      }
    }
  }
})();

/**=========================================================
 * Component: access-register.js
 * Demo for register account api
 =========================================================*/

(function () {
  'use strict';

  angular
    .module('app.auth')
    .controller('AuthRecoverCtrl', AuthRecoverCtrl);


  AuthRecoverCtrl.$inject = ['$rootScope', '$scope', 'AuthService', '$state'];

  function AuthRecoverCtrl($rootScope, $scope, AuthService, $state) {

    var vm = this;
    activate();

    ////////////////

    function activate() {
      // bind here all data from the form
      vm.email = '';
      // place the message if something goes wrong
      vm.errMsg = '';
      vm.okMsg = '';

      // set form data
      setFormData();

      function modifyBody(data) {
        var body = {
          email: data
        };
        return body
      }

      vm.resetPassword = function () {
        console.log('resetPassword: for ', vm.email)
        vm.errMsg = '';
        vm.okMsg = '';
        $rootScope.auth.popup.showPopup = false

        if (vm.recoverForm.$valid) {
          // console.log('vm.account: ', vm.account)
          var body = modifyBody(vm.email)

          AuthService.resetPassword(body).then(
            function (data) {
              //vm.email = ''
              vm.recoverForm.email.$dirty = false
              //vm.okMsg = data
              $rootScope.auth.popup.showPopup = true
              $rootScope.auth.popup.receiverEmail = vm.email
            }
            , function (err) {
              if (err) {
                if (err.message) {
                  vm.errMsg = err.message
                } else {
                  if (_.isString(err)) {
                    vm.errMsg = err
                  } else {
                    vm.errMsg = "Unable to Reset Your Password "
                  }
                }
              } else {
                vm.errMsg = "Server Request Error"
              }
            }
          )
        }
        else {
          // set as dirty if the user click directly to login so we show the validation messages
          /*jshint -W106*/
          vm.recoverForm.email.$dirty = true;
        }
      }

      function setFormData() {
        $rootScope.auth = {
          form: {
            title: {
              name: 'FORGOT YOUR PASSWORD?',
              description: 'Enter your current email and we will send you a reset link',
              hasIcon: false,
              icon: ''
            }
          },
          popup: {
            showPopup: false,
            title: 'RESET YOUR PASSWORD',
            receiverEmail: '',
            okUri: 'page.login',
            okText: 'SIGN IN',
            resendLink: function () {
              vm.resetPassword()
            }
          }
        };
      }
    }
  }
})();

/**=========================================================
 * Component: access-register.js
 * Demo for register account api
 =========================================================*/

(function () {
  'use strict';

  angular
    .module('app.auth')
    .controller('AuthRegisterCtrl', AuthRegisterCtrl)

  AuthRegisterCtrl.$inject = ['$rootScope', '$scope', 'AuthService', '$state'];
  function AuthRegisterCtrl($rootScope, $scope, AuthService, $state) {
    var vm = this;
    vm.updatePlaceData = updatePlaceData
    /*
     GOOGLE PLACES scope
     auto complete
     */
    vm.ac_search = '' //searchfield
    vm.ac_details = null // google place data will be here
    vm.ac_options = { // options to obtain place details
      type: 'geocode',
      types: 'establishment',
      // bounds: {SWLat: 49, SWLng: -97, NELat: 50, NELng: -96},
      country: 'ca',
      language: 'eng',
      typesEnabled: false,
      boundsEnabled: false,
      componentEnabled: false,
      watchEnter: true
    };
    $scope.$watch('vm.ac_details', function (data) {
      if (data) {
        vm.account.place_data = {}
        console.log('place details: ', data)
        var addr = {
          line_1: "",
          line_2: "",
          city: "",
          region: "",
          country: "",
          postal_code: "",
          place_id: data.place_id
        }
        _.each(data.address_components, function (el, key, list) {
          if (el.types) {
            if (el.types.indexOf("route") != -1) addr.line_1 = addr.line_1 + " " + el.long_name
            if (el.types.indexOf("street_number") != -1) addr.line_1 = el.long_name + addr.line_1
            if (el.types.indexOf("administrative_area_level_1") != -1) addr.region += el.long_name
            if (el.types.indexOf("country") != -1) addr.country += el.long_name
            if (el.types.indexOf("postal_code") != -1) addr.postal_code += el.long_name
            // sometimes city comes as sublocality and sometimes as locality
            if (el.types.indexOf("sublocality") != -1 && el.types.indexOf("sublocality_level_1") != -1) addr.city = el.long_name
            if (el.types.indexOf("locality") != -1 && el.types.indexOf("political") != -1) addr.city = el.long_name
          }
        });
        vm.account.place_data.address = addr
        vm.updatePlaceData(data)
      }
    })


    function updatePlaceData(data) {
      console.log('updatePlaceData: ')
      if (data) {
        var merge_data = {
          opening_hours: _.omit(data.opening_hours, 'open_now')
          , place_id: data.place_id
          , website: data.website
          , name: data.name
          , phone: data.formatted_phone_number
          , formatted_address: data.formatted_address
          , international_phone_number: data.international_phone_number
        }

        if (data.geometry && data.geometry.location) {
          if (data.geometry.location.lat) merge_data.lat = data.geometry.location.lat()
          if (data.geometry.location.lng) merge_data.lng = data.geometry.location.lng()
        }
        vm.account.place_data = _.extend(vm.account.place_data, merge_data);
      }
    }

    activate();

    function geolocate() {
      if (navigator.geolocation) {
        /*navigator.geolocation.getCurrentPosition(function(position) {
         var geolocation = {
         lat: position.coords.latitude,
         lng: position.coords.longitude
         };
         var circle = new google.maps.Circle({
         center: geolocation,
         radius: position.coords.accuracy
         });
         vm.ac_options.bounds = (circle.getBounds());
         });*/
      }
    }

    ////////////////
    function modifyBody(data) {
      return data
    }

    function activate() {
      geolocate()
      // bind here all data from the form
      vm.account = {};
      // place the message if something goes wrong
      vm.errMsg = '';
      vm.okMsg = "";

      // set form data
      setFormData();

      vm.register = function () {
        vm.errMsg = '';
        vm.okMsg = "";
        $rootScope.auth.popup.showPopup = false

        if (vm.registerForm.$valid) {
          var body = modifyBody(vm.account);
          console.log('body: ', body)
          AuthService.register(body).then(
            function (payload) {
              // $rootScope.$emit('$login', payload)
              // $state.go('app.dashboard');
              // vm.okMsg = "We have sent you an email with instructions to activate your profile"
              console.log('signup ok')
              $rootScope.auth.popup.showPopup = true
              $rootScope.auth.popup.receiverEmail = vm.account.email;
            }, function (err) {
              if (err) {
                if (err.message) {
                  vm.errMsg = err.message
                } else {
                  if (_.isString(err)) {
                    vm.errMsg = err
                  } else {
                    vm.errMsg = "Unable to Register New Accout"
                  }
                }
              } else {
                vm.errMsg = "Server Request Error"
              }
            }
          )
        } else {
          // set as dirty if the user click directly to login so we show the validation messages
          /*jshint -W106*/
          vm.registerForm.first_name.$dirty = true;
          vm.registerForm.last_name.$dirty = true;
          vm.registerForm.email.$dirty = true;
          vm.registerForm.email_confirm.$dirty = true;
          vm.registerForm.place_data.$dirty = true;
          vm.registerForm.terms_accepted.$dirty = true;

        }
      };

      vm.resendVerify = function () {
        vm.errMsg = '';
        vm.okMsg = "";
        $rootScope.auth.popup.showPopup = false
        AuthService.getVerified(vm.account.email).then(
          function (payload) {
            // $rootScope.$emit('$login', payload)
            // $state.go('app.dashboard');
            // vm.okMsg = "We have sent you an email with instructions to activate your profile"
            $rootScope.auth.popup.showPopup = true
            $rootScope.auth.popup.receiverEmail = vm.account.email;
          }, function (err) {
            if (err) {
              if (err.message) {
                vm.errMsg = err.message
              } else {
                if (_.isString(err)) {
                  vm.errMsg = err
                } else {
                  vm.errMsg = "Unable to Resend email"
                }
              }
            } else {
              vm.errMsg = "Server Request Error"
            }
          }
        )
      };

      function setFormData() {
        $rootScope.auth = {
          form: {
            title: {
              name: 'ADD YOUR PERSONAL INFO',
              description: '',
              hasIcon: false,
              icon: ''
            }
          },
          popup: {
            showPopup: false,
            title: 'VERIFY YOUR ACCOUNT',
            receiverEmail: '',
            okUri: 'page.login',
            okText: 'SIGN IN',
            resendLink: function () {
              vm.resendVerify()
            }
          }
        };
      }
    }
  }

})();

/**=========================================================
 * Component: access-register.js
 * Demo for register account api
 =========================================================*/

(function () {
  'use strict';

  angular
    .module('app.auth')
    .controller('AuthResetCtrl', AuthResetCtrl)

  AuthResetCtrl.$inject = ['$rootScope', '$scope', '$state', '$stateParams', 'AuthService', 'InviteService'];
  function AuthResetCtrl($rootScope, $scope, $state, $stateParams, AuthService, InviteService) {
    var vm = this;
    console.log('stateParams: ', $stateParams)
    activate();

    ////////////////
    function modifyBody(data) {
      return data
    }

    function activate() {
      // bind here all data from the form
      vm.account = {};
      // place the message if something goes wrong
      vm.errMsg = '';
      vm.invalidMsg = '';

      // set form data
      setFormData();

      if ($stateParams.token) {
        AuthService.findByPassResetToken($stateParams.token)
          .then(
            function (data) {
              vm.password_owner = data
              return data;
            },
            function (err) {
              vm.invalidMsg = err
            }
          );
      } else if ($stateParams.i) {
        InviteService.get($stateParams.i).then(
          function (res) {
            // vm.invite = res.data
            vm.password_owner = _.pick(res.data, 'first_name', 'last_name', 'email');
            console.log('invite data: ', vm.password_owner)
          },
          function (err) {
            vm.invalidMsg = _.isString(err) ? err : "Invite not found or not available anymore"
          }
        )
      } else {
        vm.invalidMsg = "Invalid or broken link"
      }
      function setPassword(body) {
        vm.errMsg = ""
        AuthService.setPassword($stateParams.token, body).then(
          function (payload) {
            $state.go('page.login');
          }, function (err) {
            if (err) {
              if (err.message) {
                vm.errMsg = err.message
              } else {
                if (_.isString(err)) {
                  vm.errMsg = err
                } else {
                  vm.errMsg = "Unable to Register New Accout"
                }
              }
            } else {
              vm.errMsg = "Server Request Error"
            }
          }
        )
      }

      function acceptInvite(body) {
        vm.errMsg = ""
        InviteService.accept($stateParams.i, body).then(
          function (res) {
            $rootScope.$emit('$login', res.data)
            $state.go('app.dashboard');
            // $state.go('page.login')
          },
          function (res) {
            var err = res.data
            console.log('error: ', err)
            vm.errMsg = _.isString(err) ? err : (err.message || "Unable to accept invitation")
          }
        )
      }

      vm.setPassword = function () {
        vm.errMsg = '';

        if (vm.passwordForm.$valid) {
          var body = modifyBody(vm.account);

          if ($stateParams.token) {
            setPassword(body)
          } else if ($stateParams.i) {
            acceptInvite(body)
          } else {
            console.log('something goes wrong')
          }

        } else {
          // set as dirty if the user click directly to login so we show the validation messages
          /*jshint -W106*/
          // vm.passwordForm.account_email.$dirty = true;
          vm.passwordForm.account_password.$dirty = true;
          vm.passwordForm.account_password_confirm.$dirty = true;
          // vm.passwordForm.agree.$dirty = true;
          // vm.passwordForm.account_terms_accepted.$dirty = true;
          // vm.passwordForm.account_first_name.$dirty = true;
          // vm.passwordForm.account_last_name.$dirty = true;

        }
      };

      function setFormData() {
        $rootScope.auth = {
          form: {
            title: {
              name: 'CHOOSE YOUR PASSWORD',
              description: '',
              hasIcon: false,
              icon: ''
            }
          },
          popup: {
            showPopup: false,
            title: 'RESET YOUR PASSWORD',
            receiverEmail: vm.email,
            okUri: 'page.login',
            okText: 'SIGN IN'
          }
        };
      }
    }
  }

})();

/**=========================================================
 * Component: access-register.js
 * Demo for register account api
 =========================================================*/

(function () {
  'use strict';

  angular
    .module('app.auth')
    .controller('AuthVerifyCtrl', AuthVerifyCtrl)

  AuthVerifyCtrl.$inject = ['$rootScope', '$scope', '$state', '$stateParams', 'AuthService', 'InviteService'];
  function AuthVerifyCtrl($rootScope, $scope, $state, $stateParams, AuthService, InviteService) {
    var vm = this;
    console.log('stateParams: ', $stateParams)
    activate();

    ////////////////
    function modifyBody(data) {
      var body = {
        verification: $stateParams.verification,
        password: data.password
      }
      return body
    }

    function activate() {
      // bind here all data from the form
      vm.account = {};
      // place the message if something goes wrong
      vm.errMsg = '';
      vm.invalidMsg = '';
      // set form data
      setFormData();
      if ($stateParams.verification) {
        vm.invalidMsg = '';
      } else {
        vm.invalidMsg = "Invalid or broken link"
      }
      function verify(body) {
        AuthService.verify(body).then(
          function (payload) {
            if (payload.user && payload.authorization) {
              $rootScope.$emit('$login', payload)
              $state.go('app.dashboard');
            } else {
              $state.go('page.login');
            }

          }, function (err) {
            console.log('Error: ', err);
            if (err) {
              if (err.message) {
                vm.errMsg = err.message
              } else {
                if (_.isString(err)) {
                  vm.errMsg = err
                } else {
                  vm.errMsg = "Unable to Verify Your Account"
                }
              }
            } else {
              vm.errMsg = "Server Request Error"
            }
          }
        )
      }

      vm.verify = function () {
        vm.errMsg = '';

        if (vm.passwordForm.$valid) {
          if ($stateParams.verification) {
            var body = modifyBody(vm.account);
            console.log('body: ', body)
            verify(body)
          } else {
            console.log('something goes wrong')
          }

        } else {
          vm.passwordForm.account_password.$dirty = true;
          vm.passwordForm.account_password_confirm.$dirty = true;
          vm.passwordForm.agree.$dirty = true;
        }
      }

      function setFormData() {
        $rootScope.auth = {
          form: {
            title: {
              name: 'CHOOSE YOUR PASSWORD',
              description: '',
              hasIcon: false,
              icon: ''
            }
          },
          popup: {
            showPopup: false,
            title: '',
            receiverEmail: null,
            okUri: 'page.login',
            okText: ''
          }
        };
      }
    }
  }

})();
(function() {
    'use strict';

    angular
        .module('app.auth')
        .config(authConfig);

    authConfig.$inject = ['$controllerProvider', '$compileProvider', '$filterProvider', '$provide', '$animateProvider'];
    function authConfig($controllerProvider, $compileProvider, $filterProvider, $provide, $animateProvider){

      var auth = angular.module('app.auth');
      // registering components after bootstrap
      auth.controller = $controllerProvider.register;
      auth.directive  = $compileProvider.directive;
      auth.filter     = $filterProvider.register;
      auth.factory    = $provide.factory;
      auth.service    = $provide.service;
      auth.constant   = $provide.constant;
      auth.value      = $provide.value;
      // Disables animation on items with class .ng-no-animation
      
      $animateProvider.classNameFilter(/^((?!(ng-no-animation)).)*$/);

    }

})();
/**=========================================================
 * Component: constants.js
 * Define constants to inject across the application
 =========================================================*/
(function() {
    'use strict';

    angular
        .module('app.auth')
        .constant('AUTH_CFG', {
          'endpoint': apiEndPoint+'/auth',
          'auth_fields': {
            'user': 'user',
            'authorization': 'authorization'
          }
        })
})();


(function () {
  'use strict';

  angular
    .module('app.auth')
    .run(authRun);

  authRun.$inject = ['AUTH_CFG', '$rootScope', '$state', '$stateParams', '$window', '$templateCache', 'Colors', 'AuthService', '$localStorage', '$http'];

  function authRun(AUTH_CFG, $rootScope, $state, $stateParams, $window, $templateCache, Colors, AuthService, $localStorage, $http) {



    // console.log("$localStorage.user: ", $localStorage.user)
    $rootScope.brandName = "menuCLOUD"
    // initializing session on angular app from localStorage
    // and fetch user profile data
    function clearSession() {
      $localStorage.$reset()
      $rootScope.user = void(0)
      $rootScope.initialzed = true
      $http.defaults.headers.common["Authorization"] = void(0)
    }

    function logout() {
      AuthService.logout()
    }

    function initAuth() {
      console.log('initAuth')
      if ($localStorage.authorization && $localStorage.user) {
        $rootScope.user = $localStorage.user
        $http.defaults.headers.common["Authorization"] = $localStorage.authorization
        fetchUserProfile()
      } else {
        console.log('reset $localStorage')
        clearSession()
        // $rootScope.$emit("$logout")
      }
    }

    // INITIALIZE APP SESSION
    initAuth()
    $rootScope.$on('$updateUserProfile', function (event, user) {
      updateAuth(user)
    })

    function fetchUserProfile() {
      AuthService.me().then(
        function (user) {
          console.log('init user: ', user)
          updateAuth(user)
          $rootScope.initialzed = true
        }
        , function (err, status) {
          // console.log('status: ', status)
          clearSession()
        }
      )
    }

    function updateUserProfile(user) {
      $rootScope.user = $localStorage.user = user;

    }

    function updateAuth(user, token) {
      if (user) updateUserProfile(user)
      if (token) $localStorage.authorization = $http.defaults.headers.common["Authorization"] = token
      if (!user && !token) {
        $rootScope.$emit('$logout')
      }
    }

    $rootScope.$on('$login', function (event, payload) {
      // console.log('on login')
      var user = payload[AUTH_CFG.auth_fields.user]
      var authorization = payload[AUTH_CFG.auth_fields.authorization]
      updateAuth(user, authorization)
      $rootScope.initialzed = true
    });

    /*$rootScope.$on("$stateChangeStart", function (event, toState) {
     // console.log('toState: ', toState)
     // console.log('arguments: ', arguments)
     if ((!toState.data || !toState.data.public) && !$localStorage.authorization) {
     $localStorage.$reset()
     event.preventDefault();
     $state.go("401");
     }
     });*/

    $rootScope.$on('$unauthorized', function (event) {
      console.log('$unauthorized')
      event.preventDefault();
      // $state.go("401");
      $rootScope.initialzed = true
      // window.location.href = "/#/unauthorized"
    })
    $rootScope.$logout = function () {
      $rootScope.$emit('$logout')
    }
    $rootScope.$on('$logout', function (event) {
      var htmlText = "<span class='profile-img-text'>"+$rootScope.user.first_name.charAt(0).toUpperCase()+$rootScope.user.last_name.charAt(0).toUpperCase()+"</span>"+
                     $rootScope.user.first_name + ' ' + $rootScope.user.last_name;
      swal({
        title: "DO YOU WANT TO SIGN OUT?",
        text: htmlText,
        imageUrl: "app/img/user-photo-alert.png",
        customClass: "app-alert has-close-button no-fieldset",
        showCancelButton: true,
        confirmButtonColor: "#5bc0de",
        confirmButtonText: "SIGN OUT",
        cancelButtonText: "",
        allowOutsideClick: true,
        html: true
      }, function () {
        console.log('click sign out');
        clearSession()
        $state.go('page.login')
      });
    });

  }

})();


(function () {
  'use strict';

  angular
    .module('app.auth')
    .service('AuthService', AuthService);

  AuthService.$inject = ['AUTH_CFG', '$http', '$q', '$localStorage', '$httpParamSerializer', '$rootScope'];

  function AuthService(AUTH_CFG, $http, $q, $localStorage, $httpParamSerializer, $rootScope) {

    this.register = register
    this.login = login
    this.me = me
    this.resetPassword = resetPassword
    this.findByPassResetToken = findByPassResetToken
    this.setPassword = setPassword
    this.getVerified = getVerified
    this.logout = logout
    this.verify = verify
    this.isAuthenticated = isAuthenticated
    this.changePassword = changePassword
    // serialize object to query string
    function _buildPath(endpoint, query) {
      if (!_.isEmpty(query)) {
        return endpoint + "?" + $httpParamSerializer(query)
      } else {
        return endpoint
      }
    }

    //////////////////////////////

    function login(body) {
      var deferred = $q.defer();
      var path = _buildPath(AUTH_CFG.endpoint + '/login')
      var promise = $http.post(path, body)
        .success(function (data) {
          deferred.resolve(data);
        })
        .error(function (err, status, headers, config) {
          // console.log('response: ',  arguments)
          if (status == 403) deferred.notify(status)
          deferred.reject(err);
        });

      return deferred.promise;
    }

    function register(body) {
      var deferred = $q.defer();
      var path = _buildPath(AUTH_CFG.endpoint + '/register')
      var promise = $http.post(path, body)
        .success(function (data) {
          deferred.resolve(data);
        })
        .error(function (err, status, headers, config) {
          deferred.reject(err);
        });
      return deferred.promise;
    };
    function resetPassword(body) {
      var deferred = $q.defer();
      var path = _buildPath(AUTH_CFG.endpoint + '/resetPassword')
      var promise = $http.post(path, body)
        .success(function (data) {
          deferred.resolve(data);
        })
        .error(function (err, status, headers, config) {
          deferred.reject(err);
        });
      return deferred.promise;
    };
    function findByPassResetToken(token) {
      var deferred = $q.defer();
      var path = _buildPath(AUTH_CFG.endpoint + '/findByPassResetToken', {token: token})
      var promise = $http.get(path)
        .success(function (data) {
          deferred.resolve(data);
        })
        .error(function (err, status, headers, config) {
          deferred.reject(err);
        });
      return deferred.promise;
    };
    function setPassword(token, body) {
      var deferred = $q.defer();
      var path = _buildPath(AUTH_CFG.endpoint + '/setPassword', {token: token})
      var promise = $http.post(path, body)
        .success(function (data) {
          deferred.resolve(data);
        })
        .error(function (err, status, headers, config) {
          deferred.reject(err);
        });
      return deferred.promise;
    };
    // request verification link again
    function getVerified(email) {
      var deferred = $q.defer();
      var path = _buildPath(AUTH_CFG.endpoint + '/getVerified', {email: email})
      var promise = $http.get(path)
        .success(function (data) {
          deferred.resolve(data);
        })
        .error(function (err, status, headers, config) {
          deferred.reject(err);
        });
      return deferred.promise;
    };
    // that action used only from link in activation email
    /*
     @body: {
     verification: string
     password: 8-32 signs
     }
     */
    function verify(body) {
      var deferred = $q.defer();
      var path = _buildPath(AUTH_CFG.endpoint + '/verify')
      var promise = $http.put(path, body)
        .success(function (data) {
          deferred.resolve(data);
        })
        .error(function (err, status, headers, config) {
          deferred.reject(err);
        });
      return deferred.promise;
    };
    /*
     @body: {
     password_new: 8-32 signs
     password: 8-32 signs
     }
     */
    function changePassword(body) {
      var deferred = $q.defer();
      var path = _buildPath(AUTH_CFG.endpoint + '/changePassword')
      var promise = $http.post(path, body)
        .success(function (data) {
          deferred.resolve(data);
        })
        .error(function (err, status, headers, config) {
          deferred.reject(err);
        });
      return deferred.promise;
    }
    
    function isAuthenticated() {
      return !_.isEmpty($localStorage.authorization) && _.isEmpty($localStorage.user);
    }

    function me() {
      var deferred = $q.defer();
      var path = _buildPath(AUTH_CFG.endpoint + '/me')
      var promise = $http.get(path)
        .success(function (data) {
          deferred.resolve(data);
        })
        .error(function (err, status, headers, config) {
          deferred.reject(err);
        });
      return deferred.promise;
    };
    // not implemented on api yet
    function logout() {
      console.log('logout not implemented on API side')
    }
  }
})();
(function() {
    'use strict';

    angular
        .module('app.colors')
        .constant('APP_COLORS', {
          'primary':                '#5d9cec',
          'success':                '#27c24c',
          'info':                   '#23b7e5',
          'warning':                '#ff902b',
          'danger':                 '#f05050',
          'inverse':                '#131e26',
          'green':                  '#37bc9b',
          'pink':                   '#f532e5',
          'purple':                 '#7266ba',
          'dark':                   '#3a3f51',
          'yellow':                 '#fad732',
          'gray-darker':            '#232735',
          'gray-dark':              '#3a3f51',
          'gray':                   '#dde6e9',
          'gray-light':             '#e4eaec',
          'gray-lighter':           '#edf1f2'
        })
        ;
})();
/**=========================================================
 * Component: colors.js
 * Services to retrieve global colors
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.colors')
        .service('Colors', Colors);

    Colors.$inject = ['APP_COLORS'];
    function Colors(APP_COLORS) {
        this.byName = byName;

        ////////////////

        function byName(name) {
          return (APP_COLORS[name] || '#fff');
        }
    }

})();

(function() {
    'use strict';

    angular
        .module('app.core')
        .config(coreConfig);

    coreConfig.$inject = ['$httpProvider', '$controllerProvider', '$compileProvider', '$filterProvider', '$provide', '$animateProvider'];
    function coreConfig($httpProvider, $controllerProvider, $compileProvider, $filterProvider, $provide, $animateProvider){

      var core = angular.module('app.core');
      // registering components after bootstrap
      core.controller = $controllerProvider.register;
      core.directive  = $compileProvider.directive;
      core.filter     = $filterProvider.register;
      core.factory    = $provide.factory;
      core.service    = $provide.service;
      core.constant   = $provide.constant;
      core.value      = $provide.value;

      // Disables animation on items with class .ng-no-animation
      $animateProvider.classNameFilter(/^((?!(ng-no-animation)).)*$/);
      /*
      Advanced pending request indicator
      @REASON: 
        $rootScope.processing is used to disable forms/buttons on active request
        to prevent double requests
      */
      $httpProvider.interceptors.push(["$q", "$rootScope", function($q, $rootScope) {
        var numberOfHttpRequests = 0;
        return {
            request: function (config) {
              numberOfHttpRequests += 1;
              $rootScope.processing = true;
              return config;
            },
            requestError: function (error) {
              numberOfHttpRequests -= 1;
              $rootScope.processing = (numberOfHttpRequests !== 0);
              return $q.reject(error);
            },
            response: function (response) {
              numberOfHttpRequests -= 1;
              $rootScope.processing = (numberOfHttpRequests !== 0);
              return response;
            },
            responseError: function (error) {
              numberOfHttpRequests -= 1;
              $rootScope.processing = (numberOfHttpRequests !== 0);
              return $q.reject(error);
            }
        };
      }]);
      /*
      Basic pending request indicator
      @REASON: 
        $rootScope.processing is used to disable forms/buttons on active request
        to prevent double requests
      */
      
      /*
      $http.defaults.transformRequest.push(function (data) {
        console.log('data')
        $rootScope.processing = true;
        return data;
      });
      $http.defaults.transformResponse.push(function(data){
        console.log('data')
        $rootScope.processing = false;
        return data;
      }) 
      */
    }

})();
/**=========================================================
 * Component: constants.js
 * Define constants to inject across the application
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.core')
        .constant('APP_MEDIAQUERY', {
          'desktopLG':             1200,
          'desktop':                992,
          'tablet':                 768,
          'mobile':                 480
        })
      ;

})();
(function() {
    'use strict';

    angular
        .module('app.core')
        .run(appRun);

    appRun.$inject = ['$rootScope', '$state', '$stateParams',  '$window', '$templateCache', 'Colors', '$localStorage', '$location'];
    
    function appRun($rootScope, $state, $stateParams, $window, $templateCache, Colors, $localStorage, $location) {
      
      // Set reference to access them from any scope
      $rootScope.$state = $state;
      $rootScope.$stateParams = $stateParams;
      $rootScope.$storage = $window.localStorage;
      $rootScope.$localStorage = $localStorage
      

     
      // Uncomment this to disable template cache
      /*$rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams) {
          if (typeof(toState) !== 'undefined'){
            $templateCache.remove(toState.templateUrl);
          }
      });*/

      // Allows to use branding color with interpolation
      // {{ colorByName('primary') }}
      $rootScope.colorByName = Colors.byName;

      // cancel click event easily
      $rootScope.cancel = function($event) {
        $event.stopPropagation();
      };

      // Hooks Example
      // ----------------------------------- 

      // Hook not found
      function restricted(){
        if(!$localStorage.user || !$localStorage.authorization){
          console.log('restricted')
          // $state.go('page.login')
          $location.path("/page/login").replace()
        }else{
          // ok
          return
        }
      }

      $rootScope.$on('$stateChangeStart', function (event, destState) {
        if(destState.access){
          if(!destState.access.public){
            restricted()
          }else{
            // ok
            return
          }
        }else{
          restricted()
        }
      })

      $rootScope.$on('$stateNotFound', function(event, unfoundState/*, fromState, fromParams*/) {
        console.log('$stateNotFound: ')
        console.log(unfoundState.to); // "lazy.state"
        console.log(unfoundState.toParams); // {a:1, b:2}
        console.log(unfoundState.options); // {inherit:false} + default options
      });
      // Hook error
      $rootScope.$on('$stateChangeError',
        function(event, toState, toParams, fromState, fromParams, error){
          console.log('stateChangeError:')
          console.log(error);
        });
      // Hook success
      $rootScope.$on('$stateChangeSuccess',
        function(/*event, toState, toParams, fromState, fromParams*/) {
          // display new view from top
          $window.scrollTo(0, 0);
          // Save the route title
          $rootScope.currTitle = $state.current.title;
        });

      // Load a title dynamically
      $rootScope.currTitle = $state.current.title;
      $rootScope.pageTitle = function() {
        var title = $rootScope.app.name + ' - ' + ($rootScope.currTitle || $rootScope.app.description);
        document.title = title;
        return title;
      };      

    }

})();


/**=========================================================
 * Component: fullscreen.js
 * Toggle the fullscreen mode on/off
 =========================================================*/

(function() {
    'use strict';

    angular
      .module('app.directives')
      .directive('ngThumb', ngThumb)
      
    ngThumb.$inject = ['$window'];

    function ngThumb($window) {
      var helper = {
          support: !!($window.FileReader && $window.CanvasRenderingContext2D),
          isFile: function(item) {
              return angular.isObject(item) && item instanceof $window.File;
          },
          isImage: function(file) {
              var type =  '|' + file.type.slice(file.type.lastIndexOf('/') + 1) + '|';
              return '|jpg|png|jpeg|bmp|gif|'.indexOf(type) !== -1;
          }
      };

      return {
          restrict: 'A',
          template: '<canvas/>',
          link: function(scope, element, attributes) {
              if (!helper.support) return;

              var params = scope.$eval(attributes.ngThumb);

              if (!helper.isFile(params.file)) return;
              if (!helper.isImage(params.file)) return;

              var canvas = element.find('canvas');
              var reader = new FileReader();

              reader.onload = onLoadFile;
              reader.readAsDataURL(params.file);

              function onLoadFile(event) {
                  var img = new Image();
                  img.onload = onLoadImage;
                  img.src = event.target.result;
              }

              function onLoadImage() {
                  var width = params.width || this.width / this.height * params.height;
                  var height = params.height || this.height / this.width * params.width;
                  canvas.attr({ width: width, height: height });
                  canvas[0].getContext('2d').drawImage(this, 0, 0, width, height);
              }
          }
      };
  };

})();



(function() {
    'use strict';

    angular
      .module('app.factories')
      .factory('RestFactory', RestFactory);

    RestFactory.$inject = [
      '$http'
      ,'$q'
      ,'$localStorage'
      ,'$httpParamSerializer'
      ,'$rootScope'
      ,'$sails'
    ];
    
    function RestFactory($http, $q, $localStorage, $httpParamSerializer, $rootScope, $sails) {
      
      function Factory(model_name, sails_on) {
        // console.log('Factory.model_name: ', model_name)
        this.model_name = model_name
        this.endpoint = apiBaseUrl + prefix + "/"+this.model_name
        this.get = get
        this.find = find
        this.create = create
        this.update = update
        this.destroy = destroy
        this.customAction = customAction

        // enable or disable requesting with sails socket.io
        /*
          true - use $sails provider
          false - use $http provider
        */
        this.sails = sails_on === true ? true : false
        // private methods
        this._buildPath = _buildPath

        /*
        customAction is method to call any custom actions under model_name
        arguments: (method, action, body)
        i.e.
        @method: 'get' || 'put' || 'post' || 'delete'
        @action: '/someaction'
        @body: Object @optional

        request url will look like: apiBaseUrl + prefix + "/"+this.model_name + action
        */
        function customAction(method, action, body){
          // sanitize inputs
          if(method) method = method.toLowerCase()
          if(action && action[0] != '/') action = '/'+action

          var that = this
          var deferred = $q.defer();
          var path = that._buildPath(that.endpoint + action)
          var promise
          if(that.sails_on){
            promise = $sails.get(path).then(deferred.resolve, deferred.reject)
          }else{
            var request
            if(body){
              request = $http[method](path, body)
            }else{
              request = $http[method](path)
            }
            promise = request.then(deferred.resolve, deferred.reject)
          }
          return deferred.promise;
        }

        function _buildPath(endpoint, query){
          // console.log('_buildPath args: ', arguments)
          if(!_.isEmpty(query) && _.isObject(query)){
            return endpoint+"?"+$httpParamSerializer(query)
          }else if(_.isString(query) && !_.isEmpty(query)){
            return endpoint+"?"+$httpParamSerializer({id: query})
          }else{
            return endpoint
          }
        }

        // public methods
        
        function get(id){
          var that = this
          var deferred = $q.defer();
          var path = _buildPath(that.endpoint + '/'+id)
          var promise
          if(that.sails_on){
            promise = $sails.get(path).then(deferred.resolve, deferred.reject)
          }else{
            promise = $http.get(path).then(deferred.resolve, deferred.reject)
          }
          return deferred.promise;
        };
        
        function find(query){
          console.log("find query: ", query)
          var that = this
          var deferred = $q.defer();
          var path = _buildPath(that.endpoint, query)
          var promise
          if(that.sails_on){
            promise = $sails.get(path).then(deferred.resolve, deferred.reject)
          }else{
            promise = $http.get(path).then(deferred.resolve, deferred.reject)
          }
          return deferred.promise;
        };

        function update(data){
          var that = this
          var deferred = $q.defer();
          var sanitize_data = _.omit(data, 'id')
          var path = _buildPath(that.endpoint + '/' +data.id)
          var promise
          if(that.sails_on){
            promise = $sails.put(path, sanitize_data).then(deferred.resolve, deferred.reject)
          }else{
            promise = $http.put(path, sanitize_data).then(deferred.resolve, deferred.reject)
          }
          return deferred.promise;
        }
        function create(data){
          var that = this
          var deferred = $q.defer();
          var sanitize_data = _.omit(data, 'id')
          var path = _buildPath(that.endpoint)
          var promise
          if(that.sails_on){
            promise = $sails.post(path, sanitize_data).then(deferred.resolve, deferred.reject)
          }else{
            promise = $http.post(path, sanitize_data).then(deferred.resolve, deferred.reject)
          }
          return deferred.promise;
        }
        function destroy(query){
          var that = this
          var deferred = $q.defer();
          // var sanitize_data = _.omit(data, 'id')
          var path = _buildPath(that.endpoint, query)
          var promise
          if(that.sails_on){
            promise = $sails.delete(path).then(deferred.resolve, deferred.reject)
          }else{
            promise = $http.delete(path).then(deferred.resolve, deferred.reject)
          }
          return deferred.promise; 
        }
      }

      return Factory;
    }
})();
/**=========================================================
 * Module: demo-dialog.js
 * Demo for multiple ngDialog Usage
 * - ngDialogProvider for default values not supported 
 *   using lazy loader. Include plugin in base.js instead.
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.elements')
        .controller('DialogIntroCtrl', DialogIntroCtrl)
        .controller('DialogMainCtrl', DialogMainCtrl)
        .controller('InsideCtrl', InsideCtrl)
        .controller('SecondModalCtrl', SecondModalCtrl);

    DialogIntroCtrl.$inject = ['$scope', 'ngDialog', 'tpl'];
    // Called from the route state. 'tpl' is resolved before
    function DialogIntroCtrl($scope, ngDialog, tpl) {
        
        activate();

        ////////////////

        function activate() {
          // share with other controllers
          $scope.tpl = tpl;
          // open dialog window
          ngDialog.open({
            template: tpl.path,
            // plain: true,
            className: 'ngdialog-theme-default'
          });
        }
    }

    DialogMainCtrl.$inject = ['$scope', '$rootScope', 'ngDialog'];
    // Loads from view
    function DialogMainCtrl($scope, $rootScope, ngDialog) {

        activate();

        ////////////////

        function activate() {
          $rootScope.jsonData = '{"foo": "bar"}';
          $rootScope.theme = 'ngdialog-theme-default';

          $scope.directivePreCloseCallback = function (value) {
            if(confirm('Close it? MainCtrl.Directive. (Value = ' + value + ')')) {
              return true;
            }
            return false;
          };

          $scope.preCloseCallbackOnScope = function (value) {
            if(confirm('Close it? MainCtrl.OnScope (Value = ' + value + ')')) {
              return true;
            }
            return false;
          };

          $scope.open = function () {
            ngDialog.open({ template: 'firstDialogId', controller: 'InsideCtrl', data: {foo: 'some data'} });
          };

          $scope.openDefault = function () {
            ngDialog.open({
              template: 'firstDialogId',
              controller: 'InsideCtrl',
              className: 'ngdialog-theme-default'
            });
          };

          $scope.openDefaultWithPreCloseCallbackInlined = function () {
            ngDialog.open({
              template: 'firstDialogId',
              controller: 'InsideCtrl',
              className: 'ngdialog-theme-default',
              preCloseCallback: function(value) {
                if (confirm('Close it?  (Value = ' + value + ')')) {
                  return true;
                }
                return false;
              }
            });
          };

          $scope.openConfirm = function () {
            ngDialog.openConfirm({
              template: 'modalDialogId',
              className: 'ngdialog-theme-default'
            }).then(function (value) {
              console.log('Modal promise resolved. Value: ', value);
            }, function (reason) {
              console.log('Modal promise rejected. Reason: ', reason);
            });
          };

          $scope.openConfirmWithPreCloseCallbackOnScope = function () {
            ngDialog.openConfirm({
              template: 'modalDialogId',
              className: 'ngdialog-theme-default',
              preCloseCallback: 'preCloseCallbackOnScope',
              scope: $scope
            }).then(function (value) {
              console.log('Modal promise resolved. Value: ', value);
            }, function (reason) {
              console.log('Modal promise rejected. Reason: ', reason);
            });
          };

          $scope.openConfirmWithPreCloseCallbackInlinedWithNestedConfirm = function () {
            ngDialog.openConfirm({
              template: 'dialogWithNestedConfirmDialogId',
              className: 'ngdialog-theme-default',
              preCloseCallback: function(/*value*/) {

                var nestedConfirmDialog = ngDialog.openConfirm({
                  template:
                      '<p>Are you sure you want to close the parent dialog?</p>' +
                      '<div>' +
                        '<button type="button" class="btn btn-default" ng-click="closeThisDialog(0)">No' +
                        '<button type="button" class="btn btn-primary" ng-click="confirm(1)">Yes' +
                      '</button></div>',
                  plain: true,
                  className: 'ngdialog-theme-default'
                });

                return nestedConfirmDialog;
              },
              scope: $scope
            })
            .then(function(value){
              console.log('resolved:' + value);
              // Perform the save here
            }, function(value){
              console.log('rejected:' + value);

            });
          };

          $scope.openInlineController = function () {
            $rootScope.theme = 'ngdialog-theme-default';

            ngDialog.open({
              template: 'withInlineController',
              controller: ['$scope', '$timeout', function ($scope, $timeout) {
                var counter = 0;
                var timeout;
                function count() {
                  $scope.exampleExternalData = 'Counter ' + (counter++);
                  timeout = $timeout(count, 450);
                }
                count();
                $scope.$on('$destroy', function () {
                  $timeout.cancel(timeout);
                });
              }],
              className: 'ngdialog-theme-default'
            });
          };

          $scope.openTemplate = function () {
            $scope.value = true;

            ngDialog.open({
              template: $scope.tpl.path,
              className: 'ngdialog-theme-default',
              scope: $scope
            });
          };

          $scope.openTemplateNoCache = function () {
            $scope.value = true;

            ngDialog.open({
              template: $scope.tpl.path,
              className: 'ngdialog-theme-default',
              scope: $scope,
              cache: false
            });
          };

          $scope.openTimed = function () {
            var dialog = ngDialog.open({
              template: '<p>Just passing through!</p>',
              plain: true,
              closeByDocument: false,
              closeByEscape: false
            });
            setTimeout(function () {
              dialog.close();
            }, 2000);
          };

          $scope.openNotify = function () {
            var dialog = ngDialog.open({
              template:
                '<p>You can do whatever you want when I close, however that happens.</p>' +
                '<div><button type="button" class="btn btn-primary" ng-click="closeThisDialog(1)">Close Me</button></div>',
              plain: true
            });
            dialog.closePromise.then(function (data) {
              console.log('ngDialog closed' + (data.value === 1 ? ' using the button' : '') + ' and notified by promise: ' + data.id);
            });
          };

          $scope.openWithoutOverlay = function () {
            ngDialog.open({
              template: '<h2>Notice that there is no overlay!</h2>',
              className: 'ngdialog-theme-default',
              plain: true,
              overlay: false
            });
          };

          $rootScope.$on('ngDialog.opened', function (e, $dialog) {
            console.log('ngDialog opened: ' + $dialog.attr('id'));
          });

          $rootScope.$on('ngDialog.closed', function (e, $dialog) {
            console.log('ngDialog closed: ' + $dialog.attr('id'));
          });

          $rootScope.$on('ngDialog.closing', function (e, $dialog) {
            console.log('ngDialog closing: ' + $dialog.attr('id'));
          });
        }
    
    } // DialogMainCtrl


    InsideCtrl.$inject = ['$scope', 'ngDialog'];
    function InsideCtrl($scope, ngDialog) {

        activate();

        ////////////////

        function activate() {
          $scope.dialogModel = {
            message : 'message from passed scope'
          };
          $scope.openSecond = function () {
            ngDialog.open({
              template: '<p class="lead m0"><a href="" ng-click="closeSecond()">Close all by click here!</a></h3>',
              plain: true,
              closeByEscape: false,
              controller: 'SecondModalCtrl'
            });
          };
        }
    }

    SecondModalCtrl.$inject = ['$scope', 'ngDialog'];
    function SecondModalCtrl($scope, ngDialog) {

        activate();

        ////////////////

        function activate() {
          $scope.closeSecond = function () {
            ngDialog.close();
          };
        }

    }


})();




(function() {
    'use strict';

    angular
        .module('app.loadingbar')
        .config(loadingbarConfig)
        ;
    loadingbarConfig.$inject = ['cfpLoadingBarProvider'];
    function loadingbarConfig(cfpLoadingBarProvider){
      cfpLoadingBarProvider.includeBar = true;
      cfpLoadingBarProvider.includeSpinner = false;
      cfpLoadingBarProvider.latencyThreshold = 500;
      cfpLoadingBarProvider.parentSelector = '.wrapper > section';
    }
})();
(function() {
    'use strict';

    angular
        .module('app.loadingbar')
        .run(loadingbarRun)
        ;
    loadingbarRun.$inject = ['$rootScope', '$timeout', 'cfpLoadingBar'];
    function loadingbarRun($rootScope, $timeout, cfpLoadingBar){

      // Loading bar transition
      // ----------------------------------- 
      var thBar;
      $rootScope.$on('$stateChangeStart', function() {
          if($('.wrapper > section').length) // check if bar container exists
            thBar = $timeout(function() {
              cfpLoadingBar.start();
            }, 0); // sets a latency Threshold
      });
      $rootScope.$on('$stateChangeSuccess', function(event) {
          event.targetScope.$watch('$viewContentLoaded', function () {
            $timeout.cancel(thBar);
            cfpLoadingBar.complete();
          });
      });

    }

})();
(function() {
    'use strict';

    angular
        .module('app.lazyload')
        .config(lazyloadConfig);

    lazyloadConfig.$inject = ['$ocLazyLoadProvider', 'APP_REQUIRES'];
    function lazyloadConfig($ocLazyLoadProvider, APP_REQUIRES){

      // Lazy Load modules configuration
      $ocLazyLoadProvider.config({
        debug: false,
        events: true,
        modules: APP_REQUIRES.modules
      });

    }
})();
(function() {
    'use strict';

    angular
        .module('app.lazyload')
        .constant('APP_REQUIRES', {
          // jQuery based and standalone scripts
          scripts: {
            'whirl':              ['vendor/whirl/dist/whirl.css'],
            'modernizr':          ['vendor/modernizr/modernizr.custom.js'],
            'icons':              [
              'vendor/fontawesome/css/font-awesome.min.css',
              'vendor/simple-line-icons/css/simple-line-icons.css'
            ],
            'screenfull':         ['vendor/screenfull/dist/screenfull.js'],
            'inputmask':          ['vendor/jquery.inputmask/dist/jquery.inputmask.bundle.js'],
            'ui-select': ['vendor/ui-select/dist/select.css'],
            'toaster': [
              "vendor/AngularJS-Toaster/toaster.js"
              ,"vendor/AngularJS-Toaster/toaster.css"
            ]
          },
          // Angular based script (use the right module name)
          modules: [
            // {name: 'toaster', files: ['vendor/angularjs-toaster/toaster.js', 'vendor/angularjs-toaster/toaster.css']}
            {name: 'ngDialog',                  files: ['vendor/ngDialog/js/ngDialog.min.js',
                                                       'vendor/ngDialog/css/ngDialog.min.css',
                                                       'vendor/ngDialog/css/ngDialog-theme-default.min.css'] },
          ]
        });
})();


(function() {
    'use strict';
    // Used only for the BottomSheetExample
    angular
        .module('app.material')
        .config(materialConfig)
        ;
    materialConfig.$inject = ['$mdIconProvider'];
    function materialConfig($mdIconProvider){
      $mdIconProvider
        .icon('share-arrow', 'app/img/icons/share-arrow.svg', 24)
        .icon('upload', 'app/img/icons/upload.svg', 24)
        .icon('copy', 'app/img/icons/copy.svg', 24)
        .icon('print', 'app/img/icons/print.svg', 24)
        .icon('hangout', 'app/img/icons/hangout.svg', 24)
        .icon('mail', 'app/img/icons/mail.svg', 24)
        .icon('message', 'app/img/icons/message.svg', 24)
        .icon('copy2', 'app/img/icons/copy2.svg', 24)
        .icon('facebook', 'app/img/icons/facebook.svg', 24)
        .icon('twitter', 'app/img/icons/twitter.svg', 24);
    }
})();


(function() {
    'use strict';

    angular
        .module('app.material')
        .controller('MDAutocompleteCtrl', MDAutocompleteCtrl)
        .controller('MDBottomSheetCtrl', MDBottomSheetCtrl)
        .controller('MDListBottomSheetCtrl', MDListBottomSheetCtrl)
        .controller('MDGridBottomSheetCtrl', MDGridBottomSheetCtrl)
        .controller('MDCheckboxCtrl', MDCheckboxCtrl)
        .controller('MDRadioCtrl', MDRadioCtrl)
        .controller('MDSwitchCtrl', MDSwitchCtrl)
        .controller('MDDialogCtrl', MDDialogCtrl)
        .controller('MDSliderCtrl', MDSliderCtrl)
        .controller('MDSelectCtrl', MDSelectCtrl)
        .controller('MDInputCtrl', MDInputCtrl)
        .controller('MDProgressCtrl', MDProgressCtrl)
        .controller('MDSidenavCtrl', MDSidenavCtrl)
        .controller('MDSubheaderCtrl', MDSubheaderCtrl)
        .controller('MDToastCtrl', MDToastCtrl)
          .controller('ToastCtrl', ToastCtrl)
        .controller('MDTooltipCtrl', MDTooltipCtrl)
        .controller('BottomSheetExample', BottomSheetExample)
          .controller('ListBottomSheetCtrl', ListBottomSheetCtrl)
          .controller('GridBottomSheetCtrl', GridBottomSheetCtrl)
        ;

    /*
      MDAutocompleteCtrl
     */
    MDAutocompleteCtrl.$inject = ['$scope', '$timeout', '$q'];
    function MDAutocompleteCtrl($scope, $timeout, $q) {
      var self = this;

      self.states        = loadAll();
      self.selectedItem  = null;
      self.searchText    = null;
      self.querySearch   = querySearch;
      self.simulateQuery = false;
      self.isDisabled    = false;

      // use $timeout to simulate remote dataservice call
      function querySearch (query) {
        var results = query ? self.states.filter( createFilterFor(query) ) : [],
            deferred;
        if (self.simulateQuery) {
          deferred = $q.defer();
          $timeout(function () { deferred.resolve( results ); }, Math.random() * 1000, false);
          return deferred.promise;
        } else {
          return results;
        }
      }

      function loadAll() {
        var allStates = 'Alabama, Alaska, Arizona, Arkansas, California, Colorado, Connecticut, Delaware, Florida, Georgia, Hawaii, Idaho, Illinois, Indiana, Iowa, Kansas, Kentucky, Louisiana, Maine, Maryland, Massachusetts, Michigan, Minnesota, Mississippi, Missouri, Montana, Nebraska, Nevada, New Hampshire, New Jersey, New Mexico, New York, North Carolina, North Dakota, Ohio, Oklahoma, Oregon, Pennsylvania, Rhode Island, South Carolina, South Dakota, Tennessee, Texas, Utah, Vermont, Virginia, Washington, West Virginia, Wisconsin, Wyoming';

        return allStates.split(/, +/g).map( function (state) {
          return {
            value: state.toLowerCase(),
            display: state
          };
        });
      }

          /**
           * Create filter function for a query string
           */
          function createFilterFor(query) {
            var lowercaseQuery = angular.lowercase(query);

            return function filterFn(state) {
              return (state.value.indexOf(lowercaseQuery) === 0);
            };

          }
        }

    /*
    MDBottomSheetCtrl
     */
    MDBottomSheetCtrl.$inject = ['$scope', '$timeout', '$mdBottomSheet'];
    function MDBottomSheetCtrl($scope, $timeout, $mdBottomSheet) {
      $scope.alert = '';

      $scope.showListBottomSheet = function($event) {
        $scope.alert = '';
        $mdBottomSheet.show({
          templateUrl: 'bottom-sheet-list-template.html',
          controller: 'ListBottomSheetCtrl',
          targetEvent: $event
        }).then(function(clickedItem) {
          $scope.alert = clickedItem.name + ' clicked!';
        });
      };

      $scope.showGridBottomSheet = function($event) {
        $scope.alert = '';
        $mdBottomSheet.show({
          templateUrl: 'bottom-sheet-grid-template.html',
          controller: 'GridBottomSheetCtrl',
          targetEvent: $event
        }).then(function(clickedItem) {
          $scope.alert = clickedItem.name + ' clicked!';
        });
      };
    }
    /*
    MDListBottomSheetCtrl
     */
    MDListBottomSheetCtrl.$inject = ['$scope', '$mdBottomSheet'];
    function MDListBottomSheetCtrl($scope, $mdBottomSheet) {

      $scope.items = [
        { name: 'Share', icon: 'share' },
        { name: 'Upload', icon: 'upload' },
        { name: 'Copy', icon: 'copy' },
        { name: 'Print this page', icon: 'print' },
      ];

      $scope.listItemClick = function($index) {
        var clickedItem = $scope.items[$index];
        $mdBottomSheet.hide(clickedItem);
      };
    }
    /*
    MDGridBottomSheetCtrl
     */
    MDGridBottomSheetCtrl.$inject = ['$scope', '$mdBottomSheet'];
    function MDGridBottomSheetCtrl($scope, $mdBottomSheet) {

      $scope.items = [
        { name: 'Hangout', icon: 'hangout' },
        { name: 'Mail', icon: 'mail' },
        { name: 'Message', icon: 'message' },
        { name: 'Copy', icon: 'copy' },
        { name: 'Facebook', icon: 'facebook' },
        { name: 'Twitter', icon: 'twitter' },
      ];

      $scope.listItemClick = function($index) {
        var clickedItem = $scope.items[$index];
        $mdBottomSheet.hide(clickedItem);
      };
    }
    /*
    MDCheckboxCtrl
     */
    MDCheckboxCtrl.$inject = ['$scope'];
    function MDCheckboxCtrl($scope) {

      $scope.data = {};
      $scope.data.cb1 = true;
      $scope.data.cb2 = false;
      $scope.data.cb3 = false;
      $scope.data.cb4 = false;
      $scope.data.cb5 = false;
    }
    /*
    MDRadioCtrl
     */
    MDRadioCtrl.$inject = ['$scope'];
    function MDRadioCtrl($scope) {

        $scope.data = {
          group1 : 'Banana',
          group2 : '2',
          group3 : 'avatar-1'
        };

        $scope.avatarData = [{
            id: 'svg-1',
            title: 'avatar 1',
            value: 'avatar-1'
          },{
            id: 'svg-2',
            title: 'avatar 2',
            value: 'avatar-2'
          },{
            id: 'svg-3',
            title: 'avatar 3',
            value: 'avatar-3'
        }];

        $scope.radioData = [
          { label: 'Apple', value: 1 },
          { label: 'Banana', value: 2 },
          { label: 'Mango', value: '3', isDisabled: true }
        ];


        $scope.submit = function() {
          alert('submit');
        };

        var vals = ['Apple', 'Banana', 'Mango', 'Grape', 'Melon', 'Strawberry', 'Kiwi'];
        $scope.addItem = function() {
          var rval = vals[Math.floor(Math.random() * vals.length)];
          $scope.radioData.push({ label: rval, value: rval });
        };

        $scope.removeItem = function() {
          $scope.radioData.pop();
        };
    }
    /*
    MDSwitchCtrl
     */
    MDSwitchCtrl.$inject = ['$scope'];
    function MDSwitchCtrl($scope) {
      $scope.data = {
        cb1: true,
        cb4: true
      };
      
      $scope.onChange = function(cbState){
         $scope.message = 'The switch is now: ' + cbState;
      };
    }
    /*
    MDDialogCtrl
     */
    MDDialogCtrl.$inject = ['$scope', '$mdDialog'];
    function MDDialogCtrl($scope, $mdDialog) {
      $scope.alert = '';

      $scope.showAlert = function(ev) {
        $mdDialog.show(
          $mdDialog.alert()
            .title('This is an alert title')
            .content('You can specify some description text in here.')
            .ariaLabel('Password notification')
            .ok('Got it!')
            .targetEvent(ev)
        );
      };

      $scope.showConfirm = function(ev) {
        var confirm = $mdDialog.confirm()
          .title('Would you like to delete your debt?')
          .content('All of the banks have agreed to forgive you your debts.')
          .ariaLabel('Lucky day')
          .ok('Please do it!')
          .cancel('Sounds like a scam')
          .targetEvent(ev);

        $mdDialog.show(confirm).then(function() {
          $scope.alert = 'You decided to get rid of your debt.';
        }, function() {
          $scope.alert = 'You decided to keep your debt.';
        });
      };

      $scope.showAdvanced = function(ev) {
        $mdDialog.show({
          controller: DialogController,
          templateUrl: 'dialog1.tmpl.html',
          targetEvent: ev,
        })
        .then(function(answer) {
          $scope.alert = 'You said the information was \'' + answer + '\'.';
        }, function() {
          $scope.alert = 'You cancelled the dialog.';
        });
      };
      DialogController.$inject = ['$scope', '$mdDialog'];
      function DialogController($scope, $mdDialog) {
        $scope.hide = function() {
          $mdDialog.hide();
        };

        $scope.cancel = function() {
          $mdDialog.cancel();
        };

        $scope.answer = function(answer) {
          $mdDialog.hide(answer);
        };
      }
    }
    /*
    MDSliderCtrl
     */
    MDSliderCtrl.$inject = ['$scope'];
    function MDSliderCtrl($scope) {

      $scope.color = {
        red: Math.floor(Math.random() * 255),
        green: Math.floor(Math.random() * 255),
        blue: Math.floor(Math.random() * 255)
      };

      $scope.rating1 = 3;
      $scope.rating2 = 2;
      $scope.rating3 = 4;

      $scope.disabled1 = 0;
      $scope.disabled2 = 70;
    }
    /*
    MDSelectCtrl
     */
    function MDSelectCtrl() {
      
      var vm = this;
      
      vm.userState = '';
      vm.states = ('AL AK AZ AR CA CO CT DE FL GA HI ID IL IN IA KS KY LA ME MD MA MI MN MS ' +
          'MO MT NE NV NH NJ NM NY NC ND OH OK OR PA RI SC SD TN TX UT VT VA WA WV WI ' +
          'WY').split(' ').map(function (state) { return { abbrev: state }; });

      vm.sizes = [
          'small (12-inch)',
          'medium (14-inch)',
          'large (16-inch)',
          'insane (42-inch)'
      ];
      vm.toppings = [
        { category: 'meat', name: 'Pepperoni' },
        { category: 'meat', name: 'Sausage' },
        { category: 'meat', name: 'Ground Beef' },
        { category: 'meat', name: 'Bacon' },
        { category: 'veg', name: 'Mushrooms' },
        { category: 'veg', name: 'Onion' },
        { category: 'veg', name: 'Green Pepper' },
        { category: 'veg', name: 'Green Olives' }
      ];
    }
    /*
    MDInputCtrl
     */
    MDInputCtrl.$inject = ['$scope'];
    function MDInputCtrl($scope) {
      $scope.user = {
        title: 'Developer',
        email: 'ipsum@lorem.com',
        firstName: '',
        lastName: '' ,
        company: 'Google' ,
        address: '1600 Amphitheatre Pkwy' ,
        city: 'Mountain View' ,
        state: 'CA' ,
        biography: 'Loves kittens, snowboarding, and can type at 130 WPM.\n\nAnd rumor has it she bouldered up Castle Craig!',
        postalCode : '94043'
      };
      $scope.project = {
        description: 'Nuclear Missile Defense System',
        clientName: 'Bill Clinton',
        rate: 500
      };
    }
    /*
    MDProgressCtrl
     */
    MDProgressCtrl.$inject = ['$scope', '$interval'];
    function MDProgressCtrl($scope, $interval) {
        $scope.mode = 'query';
        $scope.determinateValue = 30;
        $scope.determinateValue2 = 30;

        $interval(function() {
          $scope.determinateValue += 1;
          $scope.determinateValue2 += 1.5;
          if ($scope.determinateValue > 100) {
            $scope.determinateValue = 30;
            $scope.determinateValue2 = 30;
          }
        }, 100, 0, true);

        $interval(function() {
          $scope.mode = ($scope.mode === 'query' ? 'determinate' : 'query');
        }, 7200, 0, true);
    }
    /*
    MDSidenavCtrl
     */
    MDSidenavCtrl.$inject = ['$scope', '$timeout', '$mdSidenav', '$log'];
    function MDSidenavCtrl($scope, $timeout, $mdSidenav, $log) {
      $scope.toggleLeft = function() {
        $mdSidenav('left').toggle()
                          .then(function(){
                              $log.debug('toggle left is done');
                          });
      };
      $scope.toggleRight = function() {
        $mdSidenav('right').toggle()
                            .then(function(){
                              $log.debug('toggle RIGHT is done');
                            });
      };
      $scope.closeLeft = function() {
        $mdSidenav('left').close()
                          .then(function(){
                            $log.debug('close LEFT is done');
                          });

      };
      $scope.closeRight = function() {
        $mdSidenav('right').close()
                            .then(function(){
                              $log.debug('close RIGHT is done');
                            });
      };
    }
    /*
    MDSubheaderCtrl
     */
    MDSubheaderCtrl.$inject = ['$scope'];
    function MDSubheaderCtrl($scope) {
        $scope.messages = [
          {
            face : 'app/img/user/10.jpg',
            what: 'Brunch this weekend?',
            who: 'Min Li Chan',
            when: '3:08PM',
            notes: 'I\'ll be in your neighborhood doing errands'
          },
          {
            face : 'app/img/user/01.jpg',
            what: 'Brunch this weekend?',
            who: 'Min Li Chan',
            when: '3:08PM',
            notes: 'I\'ll be in your neighborhood doing errands'
          },
          {
            face : 'app/img/user/02.jpg',
            what: 'Brunch this weekend?',
            who: 'Min Li Chan',
            when: '3:08PM',
            notes: 'I\'ll be in your neighborhood doing errands'
          },
          {
            face : 'app/img/user/03.jpg',
            what: 'Brunch this weekend?',
            who: 'Min Li Chan',
            when: '3:08PM',
            notes: 'I\'ll be in your neighborhood doing errands'
          },
          {
            face : 'app/img/user/04.jpg',
            what: 'Brunch this weekend?',
            who: 'Min Li Chan',
            when: '3:08PM',
            notes: 'I\'ll be in your neighborhood doing errands'
          },
          {
            face : 'app/img/user/05.jpg',
            what: 'Brunch this weekend?',
            who: 'Min Li Chan',
            when: '3:08PM',
            notes: 'I\'ll be in your neighborhood doing errands'
          },
          {
            face : 'app/img/user/06.jpg',
            what: 'Brunch this weekend?',
            who: 'Min Li Chan',
            when: '3:08PM',
            notes: 'I\'ll be in your neighborhood doing errands'
          },
          {
            face : 'app/img/user/07.jpg',
            what: 'Brunch this weekend?',
            who: 'Min Li Chan',
            when: '3:08PM',
            notes: 'I\'ll be in your neighborhood doing errands'
          },
          {
            face : 'app/img/user/08.jpg',
            what: 'Brunch this weekend?',
            who: 'Min Li Chan',
            when: '3:08PM',
            notes: 'I\'ll be in your neighborhood doing errands'
          },
          {
            face : 'app/img/user/09.jpg',
            what: 'Brunch this weekend?',
            who: 'Min Li Chan',
            when: '3:08PM',
            notes: 'I\'ll be in your neighborhood doing errands'
          },
          {
            face : 'app/img/user/11.jpg',
            what: 'Brunch this weekend?',
            who: 'Min Li Chan',
            when: '3:08PM',
            notes: 'I\'ll be in your neighborhood doing errands'
          },
        ];
    }
    /*
    MDToastCtrl
     */
    MDToastCtrl.$inject = ['$scope', '$mdToast'];
    function MDToastCtrl($scope, $mdToast) {

      $scope.toastPosition = {
        bottom: false,
        top: true,
        left: false,
        right: true
      };

      $scope.getToastPosition = function() {
        return Object.keys($scope.toastPosition)
          .filter(function(pos) { return $scope.toastPosition[pos]; })
          .join(' ');
      };

      $scope.showCustomToast = function() {
        $mdToast.show({
          controller: 'ToastCtrl',
          templateUrl: 'toast-template.html',
          hideDelay: 60000,
          parent:'#toastcontainer',
          position: $scope.getToastPosition()
        });
      };

      $scope.showSimpleToast = function() {
        $mdToast.show(
          $mdToast.simple()
            .content('Simple Toast!')
            .position($scope.getToastPosition())
            .hideDelay(30000)
        );
      };

      $scope.showActionToast = function() {
        var toast = $mdToast.simple()
              .content('Action Toast!')
              .action('OK')
              .highlightAction(false)
              .position($scope.getToastPosition());

        $mdToast.show(toast).then(function() {
          alert('You clicked \'OK\'.');
        });
      };
    }
    /*
    ToastCtrl
     */
    ToastCtrl.$inject = ['$scope', '$mdToast'];
    function ToastCtrl($scope, $mdToast) {
      $scope.closeToast = function() {
        $mdToast.hide();
      };
    }
    /*
    MDTooltipCtrl
     */
    MDTooltipCtrl.$inject = ['$scope'];
    function MDTooltipCtrl($scope) {
      $scope.demo = {};
    }
    /*
    BottomSheetExample
     */
    BottomSheetExample.$inject = ['$scope', '$timeout', '$mdBottomSheet'];
    function BottomSheetExample($scope, $timeout, $mdBottomSheet) {
      $scope.alert = '';

      $scope.showListBottomSheet = function($event) {
        $scope.alert = '';
        $mdBottomSheet.show({
          templateUrl: 'bottom-sheet-list-template.html',
          controller: 'ListBottomSheetCtrl',
          targetEvent: $event,
          parent: '#bottomsheetcontainer'
        }).then(function(clickedItem) {
          $scope.alert = clickedItem.name + ' clicked!';
        });
      };

      $scope.showGridBottomSheet = function($event) {
        $scope.alert = '';
        $mdBottomSheet.show({
          templateUrl: 'bottom-sheet-grid-template.html',
          controller: 'GridBottomSheetCtrl',
          targetEvent: $event,
          parent: '#bottomsheetcontainer'
        }).then(function(clickedItem) {
          $scope.alert = clickedItem.name + ' clicked!';
        });
      };
    }
    /*
    ListBottomSheetCtrl
     */
    ListBottomSheetCtrl.$inject = ['$scope', '$mdBottomSheet'];
    function ListBottomSheetCtrl($scope, $mdBottomSheet) {

      $scope.items = [
        { name: 'Share', icon: 'share-arrow' },
        { name: 'Upload', icon: 'upload' },
        { name: 'Copy', icon: 'copy' },
        { name: 'Print this page', icon: 'print' },
      ];

      $scope.listItemClick = function($index) {
        var clickedItem = $scope.items[$index];
        $mdBottomSheet.hide(clickedItem);
      };
    }
    /*
    GridBottomSheetCtrl
     */
    GridBottomSheetCtrl.$inject = ['$scope', '$mdBottomSheet'];
    function GridBottomSheetCtrl($scope, $mdBottomSheet) {
      $scope.items = [
        { name: 'Hangout', icon: 'hangout' },
        { name: 'Mail', icon: 'mail' },
        { name: 'Message', icon: 'message' },
        { name: 'Copy', icon: 'copy2' },
        { name: 'Facebook', icon: 'facebook' },
        { name: 'Twitter', icon: 'twitter' },
      ];

      $scope.listItemClick = function($index) {
        var clickedItem = $scope.items[$index];
        $mdBottomSheet.hide(clickedItem);
      };
    }


})();

(function() {
    'use strict';
    // Used only for the BottomSheetExample
    angular
        .module('app.material')
        .run(materialRun)
        ;
    materialRun.$inject = ['$http', '$templateCache'];
    function materialRun($http, $templateCache){
      var urls = [
        'app/img/icons/share-arrow.svg',
        'app/img/icons/upload.svg',
        'app/img/icons/copy.svg',
        'app/img/icons/print.svg',
        'app/img/icons/hangout.svg',
        'app/img/icons/mail.svg',
        'app/img/icons/message.svg',
        'app/img/icons/copy2.svg',
        'app/img/icons/facebook.svg',
        'app/img/icons/twitter.svg'
      ];

      angular.forEach(urls, function(url) {
        $http.get(url, {cache: $templateCache});
      });

    }

})();

(function() {
    'use strict';

    angular
        .module('app.material')
        .controller('MaterialWidgetsController', MaterialWidgetsController);

    MaterialWidgetsController.$inject = ['Colors'];
    function MaterialWidgetsController(Colors) {
        var vm = this;

        activate();

        ////////////////

        function activate() {

          vm.sparkOption1 = {
            type : 'line',
            width : '100%',
            height : '140px',
            tooltipOffsetX : -20,
            tooltipOffsetY : 20,
            lineColor : Colors.byName('success'),
            fillColor : Colors.byName('success'),
            spotColor : 'rgba(0,0,0,.26)',
            minSpotColor : 'rgba(0,0,0,.26)',
            maxSpotColor : 'rgba(0,0,0,.26)',
            highlightSpotColor : 'rgba(0,0,0,.26)',
            highlightLineColor : 'rgba(0,0,0,.26)',
            spotRadius : 2,
            tooltipPrefix : '',
            tooltipSuffix : ' Visits',
            tooltipFormat : '{{prefix}}{{y}}{{suffix}}',
            chartRangeMin: 0,
            resize: true
          };

          vm.sparkOptionPie = {
            type: 'pie',
            width : '2em',
            height : '2em',
            sliceColors: [ Colors.byName('success'), Colors.byName('gray-light')]
          };
        
        }
    }
})();
/**=========================================================
 * Component: navbar-search.js
 * Navbar search toggler * Auto dismiss on ESC key
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.navsearch')
        .directive('searchOpen', searchOpen)
        .directive('searchDismiss', searchDismiss);

    //
    // directives definition
    // 
    
    function searchOpen () {
        var directive = {
            controller: searchOpenController,
            restrict: 'A'
        };
        return directive;

    }

    function searchDismiss () {
        var directive = {
            controller: searchDismissController,
            restrict: 'A'
        };
        return directive;
        
    }

    //
    // Contrller definition
    // 
    
    searchOpenController.$inject = ['$scope', '$element', 'NavSearch'];
    function searchOpenController ($scope, $element, NavSearch) {
      $element
        .on('click', function (e) { e.stopPropagation(); })
        .on('click', NavSearch.toggle);
    }

    searchDismissController.$inject = ['$scope', '$element', 'NavSearch'];
    function searchDismissController ($scope, $element, NavSearch) {
      
      var inputSelector = '.navbar-form input[type="text"]';

      $(inputSelector)
        .on('click', function (e) { e.stopPropagation(); })
        .on('keyup', function(e) {
          if (e.keyCode === 27) // ESC
            NavSearch.dismiss();
        });
        
      // click anywhere closes the search
      $(document).on('click', NavSearch.dismiss);
      // dismissable options
      $element
        .on('click', function (e) { e.stopPropagation(); })
        .on('click', NavSearch.dismiss);
    }

})();


/**=========================================================
 * Component: nav-search.js
 * Services to share navbar search functions
 =========================================================*/
 
(function() {
    'use strict';

    angular
        .module('app.navsearch')
        .service('NavSearch', NavSearch);

    function NavSearch() {
        this.toggle = toggle;
        this.dismiss = dismiss;

        ////////////////

        var navbarFormSelector = 'form.navbar-form';

        function toggle() {
          var navbarForm = $(navbarFormSelector);

          navbarForm.toggleClass('open');
          
          var isOpen = navbarForm.hasClass('open');
          
          navbarForm.find('input')[isOpen ? 'focus' : 'blur']();
        }

        function dismiss() {
          $(navbarFormSelector)
            .removeClass('open') // Close control
            .find('input[type="text"]').blur() // remove focus
            .val('') // Empty input
            ;
        }        
    }
})();

/**=========================================================
 * Collapse panels * [panel-collapse]
 =========================================================*/
(function() {
    'use strict';

    angular
        .module('app.panels')
        .directive('panelCollapse', panelCollapse);

    function panelCollapse () {
        var directive = {
            controller: Controller,
            restrict: 'A',
            scope: false
        };
        return directive;
    }

    Controller.$inject = ['$scope', '$element', '$timeout', '$localStorage'];
    function Controller ($scope, $element, $timeout, $localStorage) {
      var storageKeyName = 'panelState';

      // Prepare the panel to be collapsible
      var $elem   = $($element),
          parent  = $elem.closest('.panel'), // find the first parent panel
          panelId = parent.attr('id');

      // Load the saved state if exists
      var currentState = loadPanelState( panelId );
      if ( typeof currentState !== 'undefined') {
        $timeout(function(){
            $scope[panelId] = currentState; },
          10);
      }

      // bind events to switch icons
      $element.bind('click', function(e) {
        e.preventDefault();
        savePanelState( panelId, !$scope[panelId] );

      });
  
      // Controller helpers
      function savePanelState(id, state) {
        if(!id) return false;
        var data = angular.fromJson($localStorage[storageKeyName]);
        if(!data) { data = {}; }
        data[id] = state;
        $localStorage[storageKeyName] = angular.toJson(data);
      }
      function loadPanelState(id) {
        if(!id) return false;
        var data = angular.fromJson($localStorage[storageKeyName]);
        if(data) {
          return data[id];
        }
      }
    }

})();

/**=========================================================
 * Dismiss panels * [panel-dismiss]
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.panels')
        .directive('panelDismiss', panelDismiss);

    function panelDismiss () {

        var directive = {
            controller: Controller,
            restrict: 'A'
        };
        return directive;

    }

    Controller.$inject = ['$scope', '$element', '$q', 'Utils'];
    function Controller ($scope, $element, $q, Utils) {
      var removeEvent   = 'panel-remove',
          removedEvent  = 'panel-removed';

      $element.on('click', function (e) {
        e.preventDefault();

        // find the first parent panel
        var parent = $(this).closest('.panel');

        removeElement();

        function removeElement() {
          var deferred = $q.defer();
          var promise = deferred.promise;
          
          // Communicate event destroying panel
          $scope.$emit(removeEvent, parent.attr('id'), deferred);
          promise.then(destroyMiddleware);
        }

        // Run the animation before destroy the panel
        function destroyMiddleware() {
          if(Utils.support.animation) {
            parent.animo({animation: 'bounceOut'}, destroyPanel);
          }
          else destroyPanel();
        }

        function destroyPanel() {

          var col = parent.parent();
          parent.remove();
          // remove the parent if it is a row and is empty and not a sortable (portlet)
          col
            .filter(function() {
            var el = $(this);
            return (el.is('[class*="col-"]:not(.sortable)') && el.children('*').length === 0);
          }).remove();

          // Communicate event destroyed panel
          $scope.$emit(removedEvent, parent.attr('id'));

        }

      });
    }
})();



/**=========================================================
 * Refresh panels
 * [panel-refresh] * [data-spinner="standard"]
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.panels')
        .directive('panelRefresh', panelRefresh);

    function panelRefresh () {
        var directive = {
            controller: Controller,
            restrict: 'A',
            scope: false
        };
        return directive;

    }

    Controller.$inject = ['$scope', '$element'];
    function Controller ($scope, $element) {
      var refreshEvent   = 'panel-refresh',
          whirlClass     = 'whirl',
          defaultSpinner = 'standard';

      // catch clicks to toggle panel refresh
      $element.on('click', function (e) {
        e.preventDefault();

        var $this   = $(this),
            panel   = $this.parents('.panel').eq(0),
            spinner = $this.data('spinner') || defaultSpinner
            ;

        // start showing the spinner
        panel.addClass(whirlClass + ' ' + spinner);

        // Emit event when refresh clicked
        $scope.$emit(refreshEvent, panel.attr('id'));

      });

      // listen to remove spinner
      $scope.$on('removeSpinner', removeSpinner);

      // method to clear the spinner when done
      function removeSpinner (ev, id) {
        if (!id) return;
        var newid = id.charAt(0) === '#' ? id : ('#'+id);
        angular
          .element(newid)
          .removeClass(whirlClass);
      }
    }
})();



/**=========================================================
 * Module panel-tools.js
 * Directive tools to control panels.
 * Allows collapse, refresh and dismiss (remove)
 * Saves panel state in browser storage
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.panels')
        .directive('paneltool', paneltool);

    paneltool.$inject = ['$compile', '$timeout'];
    function paneltool ($compile, $timeout) {
        var directive = {
            link: link,
            restrict: 'E',
            scope: false
        };
        return directive;

        function link(scope, element, attrs) {

          var templates = {
            /* jshint multistr: true */
            collapse:'<a href="#" panel-collapse="" uib-tooltip="Collapse Panel" ng-click="{{panelId}} = !{{panelId}}"> \
                        <em ng-show="{{panelId}}" class="fa fa-plus ng-no-animation"></em> \
                        <em ng-show="!{{panelId}}" class="fa fa-minus ng-no-animation"></em> \
                      </a>',
            dismiss: '<a href="#" panel-dismiss="" uib-tooltip="Close Panel">\
                       <em class="fa fa-times"></em>\
                     </a>',
            refresh: '<a href="#" panel-refresh="" data-spinner="{{spinner}}" uib-tooltip="Refresh Panel">\
                       <em class="fa fa-refresh"></em>\
                     </a>'
          };

          var tools = scope.panelTools || attrs;

          $timeout(function() {
            element.html(getTemplate(element, tools )).show();
            $compile(element.contents())(scope);

            element.addClass('pull-right');
          });

          function getTemplate( elem, attrs ){
            var temp = '';
            attrs = attrs || {};
            if(attrs.toolCollapse)
              temp += templates.collapse.replace(/{{panelId}}/g, (elem.parent().parent().attr('id')) );
            if(attrs.toolDismiss)
              temp += templates.dismiss;
            if(attrs.toolRefresh)
              temp += templates.refresh.replace(/{{spinner}}/g, attrs.toolRefresh);
            return temp;
          }
        }// link
    }

})();

/**=========================================================
 * Module: demo-panels.js
 * Provides a simple demo for panel actions
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.panels')
        .controller('PanelsCtrl', PanelsCtrl);

    PanelsCtrl.$inject = ['$scope', '$timeout'];
    function PanelsCtrl($scope, $timeout) {

        activate();

        ////////////////

        function activate() {

          // PANEL COLLAPSE EVENTS
          // ----------------------------------- 

          // We can use panel id name for the boolean flag to [un]collapse the panel
          $scope.$watch('panelDemo1',function(newVal){
              
              console.log('panelDemo1 collapsed: ' + newVal);

          });


          // PANEL DISMISS EVENTS
          // ----------------------------------- 

          // Before remove panel
          $scope.$on('panel-remove', function(event, id, deferred){
            
            console.log('Panel #' + id + ' removing');
            
            // Here is obligatory to call the resolve() if we pretend to remove the panel finally
            // Not calling resolve() will NOT remove the panel
            // It's up to your app to decide if panel should be removed or not
            deferred.resolve();
          
          });

          // Panel removed ( only if above was resolved() )
          $scope.$on('panel-removed', function(event, id){

            console.log('Panel #' + id + ' removed');

          });


          // PANEL REFRESH EVENTS
          // ----------------------------------- 

          $scope.$on('panel-refresh', function(event, id) {
            var secs = 3;
            
            console.log('Refreshing during ' + secs +'s #'+id);

            $timeout(function(){
              // directive listen for to remove the spinner 
              // after we end up to perform own operations
              $scope.$broadcast('removeSpinner', id);
              
              console.log('Refreshed #' + id);

            }, 3000);

          });

          // PANELS VIA NG-REPEAT
          // ----------------------------------- 

          $scope.panels = [
            {
              id: 'panelRepeat1',
              title: 'Panel Title 1',
              body: 'Nulla eget lorem leo, sit amet elementum lorem. '
            },
            {
              id: 'panelRepeat2',
              title: 'Panel Title 2',
              body: 'Nulla eget lorem leo, sit amet elementum lorem. '
            },
            {
              id: 'panelRepeat3',
              title: 'Panel Title 3',
              body: 'Nulla eget lorem leo, sit amet elementum lorem. '
            }
          ];
        }

    } //PanelsCtrl

})();


/**=========================================================
 * Drag and drop any panel based on jQueryUI portlets
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.panels')
        .directive('portlet', portlet);

    portlet.$inject = ['$timeout', '$localStorage'];
    function portlet ($timeout, $localStorage) {
      var storageKeyName = 'portletState';

      return {
        restrict: 'A',
        link: link
      };

      /////////////

      function link(scope, element) {
          
        // not compatible with jquery sortable
        if(!$.fn.sortable) return;

        element.sortable({
          connectWith:          '[portlet]', // same like directive 
          items:                'div.panel',
          handle:               '.portlet-handler',
          opacity:              0.7,
          placeholder:          'portlet box-placeholder',
          cancel:               '.portlet-cancel',
          forcePlaceholderSize: true,
          iframeFix:            false,
          tolerance:            'pointer',
          helper:               'original',
          revert:               200,
          forceHelperSize:      true,
          update:               savePortletOrder,
          create:               loadPortletOrder
        });

      }


      function savePortletOrder(event/*, ui*/) {
        var self = event.target;
        var data = angular.fromJson($localStorage[storageKeyName]);
        
        if(!data) { data = {}; }

        data[self.id] = $(self).sortable('toArray');

        if(data) {
          $timeout(function() {
            $localStorage[storageKeyName] = angular.toJson(data);
          });
        }
      }

      function loadPortletOrder(event) {
        var self = event.target;
        var data = angular.fromJson($localStorage[storageKeyName]);

        if(data) {
          
          var porletId = self.id,
              panels   = data[porletId];

          if(panels) {
            var portlet = $('#'+porletId);
            
            $.each(panels, function(index, value) {
               $('#'+value).appendTo(portlet);
            });
          }

        }
      }

    }

})();
 
(function() {
    'use strict';

    angular
        .module('app.preloader')
        .directive('preloader', preloader);

    preloader.$inject = ['$animate', '$timeout', '$q'];
    function preloader ($animate, $timeout, $q) {

        var directive = {
            restrict: 'EAC',
            template: 
              '<div class="preloader-progress">' +
                  '<div class="preloader-progress-bar" ' +
                       'ng-style="{width: loadCounter + \'%\'}"></div>' +
              '</div>'
            ,
            link: link
        };
        return directive;

        ///////

        function link(scope, el) {

          scope.loadCounter = 0;

          var counter  = 0,
              timeout;

          // disables scrollbar
          angular.element('body').css('overflow', 'hidden');
          // ensure class is present for styling
          el.addClass('preloader');

          appReady().then(endCounter);

          timeout = $timeout(startCounter);

          ///////

          function startCounter() {

            var remaining = 100 - counter;
            counter = counter + (0.015 * Math.pow(1 - Math.sqrt(remaining), 2));

            scope.loadCounter = parseInt(counter, 10);

            timeout = $timeout(startCounter, 20);
          }

          function endCounter() {

            $timeout.cancel(timeout);

            scope.loadCounter = 100;

            $timeout(function(){
              // animate preloader hiding
              $animate.addClass(el, 'preloader-hidden');
              // retore scrollbar
              angular.element('body').css('overflow', '');
            }, 300);
          }

          function appReady() {
            var deferred = $q.defer();
            var viewsLoaded = 0;
            // if this doesn't sync with the real app ready
            // a custom event must be used instead
            var off = scope.$on('$viewContentLoaded', function () {
              viewsLoaded ++;
              // we know there are at least two views to be loaded 
              // before the app is ready (1-index.html 2-app*.html)
              if ( viewsLoaded === 2) {
                // with resolve this fires only once
                $timeout(function(){
                  deferred.resolve();
                }, 3000);

                off();
              }

            });

            return deferred.promise;
          }

        } //link
    }

})();
(function () {
  'use strict';

  angular
    .module('app.profile')
    .config(profileConfig);

  profileConfig.$inject = ['$controllerProvider', '$compileProvider', '$filterProvider', '$provide', '$animateProvider'];
  function profileConfig($controllerProvider, $compileProvider, $filterProvider, $provide, $animateProvider) {

    var profile = angular.module('app.restaurant');
    // registering components after bootstrap
    profile.controller = $controllerProvider.register;
    profile.directive = $compileProvider.directive;
    profile.filter = $filterProvider.register;
    profile.factory = $provide.factory;
    profile.service = $provide.service;
    profile.constant = $provide.constant;
    profile.value = $provide.value;
    // Disables animation on items with class .ng-no-animation

    $animateProvider.classNameFilter(/^((?!(ng-no-animation)).)*$/);

  }

})();
/**=========================================================
 * Component: business-list.controller.js
 *
 =========================================================*/

(function () {
  'use strict';

  angular
    .module('app.profile')
    .controller('ProfileCtrl', ProfileCtrl)

  ProfileCtrl.$inject = [
    '$rootScope'
    , '$scope'
    , '$state'
    , 'AuthService'
  ];

  function ProfileCtrl($rootScope, $scope, $state, AuthService) {
    var vm = this;

    activate();

    ////////////////
    function modifyBody(data) {
      var obj = {
        password: data.password,
        new_password: data.new_password
      }
      return obj;
    }

    function activate() {

      vm.account = {};
      vm.okMsg = '';
      vm.errMsg = '';

      vm.changePassword = function () {
        vm.okMsg = '';
        vm.errMsg = '';

        if (vm.passwordForm.$valid) {
          var body = modifyBody(vm.account);
          console.log('Body: ', body);
          AuthService.changePassword(body).then(function (data) {
            vm.okMsg = 'Your password has been updated!';
            $rootScope.profile.password.edit = false;
          }, function (err) {
            if (err) {
              if (err.message) {
                vm.errMsg = err.message
              } else {
                if (_.isString(err)) {
                  vm.errMsg = err
                } else {
                  vm.errMsg = "Unable to Change Password"
                }
              }
            } else {
              vm.errMsg = "Server Request Error"
            }
          });
        } else {
          vm.passwordForm.password.$dirty = true;
          vm.passwordForm.new_password.$dirty = true;
          vm.passwordForm.confirm_password.$dirty = true;
        }
      };
      
      $rootScope.topnavbar.title = 'SETTINGS';
      $rootScope.app.layout.hasSubTopNavBar = true;
      console.log('hasSubTopBar', $rootScope.app.layout.hasSubTopNavBar);
      $rootScope.tabId = 0;
      $rootScope.subtopnavbar = {
        tabId: 0,
        navBack: {
          uri: 'app.dashboard',
          title: 'Dashboard'
        },
        items: [
          {title: 'Profile', iconClass: 'icon-profile'},
          {title: 'Manage Apps', iconClass: 'icon-manage-apps'},
          {title: 'Billings', iconClass: 'icon-billing'}
        ]
      };

      $rootScope.billingInfo = {
        fields: [
          {id: 0, name: 'Date'},
          {id: 1, name: 'Payment'},
          {id: 2, name: 'App'},
          {id: 3, name: 'View'}
        ],
        data: [
          {
            date: 'June 1 / 2016',
            payment: '$20',
            app: 'menucloud'
          },{
            date: 'June 1 / 2016',
            payment: '$20',
            app: 'menucloud'
          }
        ]
      };

      $rootScope.myAppsInfo = [
        {
          selectedTab: 0,
          buttonText: 'UPGRADE',
          description: {
            logo: {
              img: 'app/img/logo5.png',
              title: ''
            },
            role: 'FREE',
            text: 'menuCLOUD is your all in one easy to use menu manager and publisher. Managae, design and publish your menus all from within one cloud based app.'
          },
          detail: [
            {
              id: 0,
              css: 'right-space',
              name: 'Your subscription',
              data: [
                {id: 0, val: 'Free'},
                {id: 1, val: '$0/month'}
              ]
            },{
              id: 0,
              css: 'has-separate',
              name: 'Added restaurants',
              data: [
                {id: 0, val: 'Restaurant one'},
                {id: 1, val: 'Restaurant two'},
                {id: 2, val: 'Restaurant three'}
              ]
            }
          ]
        },{
          selectedTab: 0,
          buttonText: 'LEARN MORE',
          description: {
            logo: {
              img: '',
              title: 'EVENTS'
            },
            role: 'COMING SOON',
            text: 'Manage your catering, guestlist and booking within one easy to use app. Easily capture and organize all your bookings for all your locations, and integrate with ease.'
          },
          detail: [
            {
              id: 0,
              css: 'right-space',
              name: 'Your subscription',
              data: [
                {id: 0, val: 'Free'},
                {id: 1, val: '$0/month'}
              ]
            },{
              id: 0,
              css: 'has-separate',
              name: 'Added restaurants',
              data: [
                {id: 0, val: 'Restaurant one'},
                {id: 1, val: 'Restaurant two'},
                {id: 2, val: 'Restaurant three'}
              ]
            }
          ]
        }
      ];

      $scope.editBillingInfo = function () {
        var htmlText =
          "<div class='form-wrapper'>" +
          "<div class='form-item'>" +
          "<input type='text' ng-model='' placeholder='Cardholders name'>" +
          "</div>" +
          "<div class='form-item'>" +
          "<input type='number' ng-model='' placeholder='Card number'>" +
          "</div>" +
          "<div class='form-item col-50 float-left icon-right icon-calendar1'>" +
          "<input type='number' ng-model='' placeholder='Year'>" +
          "</div>" +
          "<div class='form-item col-50 float-right icon-right icon-calendar1'>" +
          "<input type='number' ng-model='' placeholder='Month '>" +
          "</div>" +
          "<div class='float-clear'></div>"+
          "<div class='form-item col-50 float-left icon-right icon-calendar1'>" +
          "<input type='number' ng-model='' placeholder='CVV'>" +
          "</div>" +
          "<div class='float-clear'></div>"+
          "</div>";

        swal({
          title: "PAYMENT DETAILS",
          text: htmlText,
          imageUrl: null,
          customClass: "app-alert has-close-button form-alert no-fieldset",
          showCancelButton: true,
          confirmButtonColor: "#5bc0de",
          confirmButtonText: "Edit",
          cancelButtonText: "",
          allowOutsideClick: true,
          html: true
        }, function () {
          console.log('click edit billing info');
        });
      };
    }
  }

})();

(function () {
  'use strict';

  angular
    .module('app.profile')
    .run(profileRun);

  profileRun.$inject = ['$rootScope', '$state', '$stateParams', '$window', '$templateCache', 'Colors', 'AuthService', '$localStorage', '$http'];

  function profileRun($rootScope, $state, $stateParams, $window, $templateCache, Colors, AuthService, $localStorage, $http) {

  }

})();


(function () {
  'use strict';

  angular
    .module('app.profile')
    .service('ProfileService', ProfileService);

  ProfileService.$inject = ['$http', '$q', '$localStorage', '$httpParamSerializer', '$rootScope'];

  function ProfileService($http, $q, $localStorage, $httpParamSerializer, $rootScope) {
  }

})();
/**=========================================================
 * Component: helpers.js
 * Provides helper functions for routes definition
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.routes')
        .provider('RouteHelpers', RouteHelpersProvider)
        .service('RouteHelperService', RouteHelpersProvider)
        ;

    RouteHelpersProvider.$inject = ['APP_REQUIRES'];
    function RouteHelpersProvider(APP_REQUIRES) {

      /* jshint validthis:true */
      return {
        // provider access level
        basepath: basepath,
        resolveFor: resolveFor,
        // controller access level
        $get: function() {
          return {
            basepath: basepath,
            resolveFor: resolveFor
          };
        }
      };

      // Set here the base of the relative path
      // for all app views
      function basepath(uri) {
        return 'app/views/' + uri;
      }

      // Generates a resolve object by passing script names
      // previously configured in constant.APP_REQUIRES
      function resolveFor() {
        var _args = arguments;
        return {
          deps: ['$ocLazyLoad','$q', function ($ocLL, $q) {
            // Creates a promise chain for each argument
            var promise = $q.when(1); // empty promise
            for(var i=0, len=_args.length; i < len; i ++){
              promise = andThen(_args[i]);
            }
            return promise;

            // creates promise to chain dynamically
            function andThen(_arg) {
              // also support a function that returns a promise
              if(typeof _arg === 'function')
                  return promise.then(_arg);
              else
                  return promise.then(function() {
                    // if is a module, pass the name. If not, pass the array
                    var whatToLoad = getRequired(_arg);
                    // simple error check
                    if(!whatToLoad) return $.error('Route resolve: Bad resource name [' + _arg + ']');
                    // finally, return a promise
                    return $ocLL.load( whatToLoad );
                  });
            }
            // check and returns required data
            // analyze module items with the form [name: '', files: []]
            // and also simple array of script files (for not angular js)
            function getRequired(name) {
              if (APP_REQUIRES.modules)
                  for(var m in APP_REQUIRES.modules)
                      if(APP_REQUIRES.modules[m].name && APP_REQUIRES.modules[m].name === name)
                          return APP_REQUIRES.modules[m];
              return APP_REQUIRES.scripts && APP_REQUIRES.scripts[name];
            }

          }]};
      } // resolveFor

    }


})();


/**=========================================================
 * Component: config.js
 * App routes and resources configuration
 =========================================================*/


(function () {
  'use strict';

  angular
    .module('app.routes')
    .config(routesConfig);

  routesConfig.$inject = ['$stateProvider', '$locationProvider', '$urlRouterProvider', 'RouteHelpersProvider'];
  function routesConfig($stateProvider, $locationProvider, $urlRouterProvider, helper) {

    // Set the following to true to enable the HTML5 Mode
    // You may have to set <base> tag in index and a routing configuration in your server
    $locationProvider.html5Mode(false);

    // defaults to dashboard
    $urlRouterProvider.otherwise('/page/login');

    //
    // Application Routes
    // -----------------------------------
    $stateProvider
      .state('app', {
        url: '/app',
        abstract: true,
        templateUrl: helper.basepath('app.html'),
        resolve: helper.resolveFor('modernizr', 'icons', 'screenfull', 'ui-select', 'toaster', 'whirl')
      })

      .state('app.dashboard', {
        url: '/dashboard',
        title: 'Dashboard',
        templateUrl: helper.basepath('dashboard.html')
      })

      .state('app.submenu', {
        url: '/submenu',
        title: 'Submenu',
        templateUrl: helper.basepath('submenu.html')
      })

      .state('app.restaurant-overview', {
        url: '/restaurant-overview',
        title: 'RestaurantOverview',
        templateUrl: helper.basepath('restaurant-overview.html')
      })
      
      .state('app.restaurant', {
        url: '/restaurant',
        title: 'Restaurant',
        templateUrl: helper.basepath('restaurant/index.html')
      })

      .state('app.profile', {
        url: '/profile',
        title: 'Profile',
        templateUrl: helper.basepath('profile/index.html')
      })

      //
      // Single Page Routes
      // -----------------------------------
      .state('page', {
        url: '/page',
        templateUrl: 'app/pages/page.html',
        resolve: helper.resolveFor('modernizr', 'icons'),
        controller: ['$rootScope', function ($rootScope) {
          $rootScope.app.layout.isBoxed = false;
        }]
      })
      .state('page.login', {
        url: '/login',
        title: 'Login',
        templateUrl: 'app/pages/signin.html',
        access: {
          public: true
        }
      })
      .state('page.register', {
        url: '/register',
        title: 'Register',
        templateUrl: 'app/pages/signup.html',
        access: {
          public: true
        }
      })
      .state('page.forgot-password', {
        url: '/forgot-password',
        title: 'Forgot Password',
        templateUrl: 'app/pages/forgot-password.html',
        access: {
          public: true
        }
      })
      .state('page.reset', {
        url: '/reset?token&i',
        title: 'Reset Your Password',
        templateUrl: 'app/pages/reset-password.html',
        access: {
          public: true
        }
      })

      .state('page.verify', {
        url: '/verify?verification',
        title: 'Your email address has been verified',
        templateUrl: 'app/pages/verify.html',
        access: {
          public: true
        }
      })

      .state('page.lock', {
        url: '/lock',
        title: 'Lock',
        templateUrl: 'app/pages/lock.html',
        access: {
          public: true
        }
      })
      .state('page.404', {
        url: '/404',
        title: 'Not Found',
        templateUrl: 'app/pages/404.html',
        access: {
          public: true
        }
      })


    //
    // CUSTOM RESOLVES
    //   Add your own resolves properties
    //   following this object extend
    //   method
    // -----------------------------------
    // .state('app.someroute', {
    //   url: '/some_url',
    //   templateUrl: 'path_to_template.html',
    //   controller: 'someController',
    //   resolve: angular.extend(
    //     helper.resolveFor(), {
    //     // YOUR RESOLVES GO HERE
    //     }
    //   )
    // })
    ;

  } // routesConfig

})();


(function() {
    'use strict';

    angular
        .module('app.settings')
        .run(settingsRun);

    settingsRun.$inject = ['$rootScope', '$localStorage'];

    function settingsRun($rootScope, $localStorage){

      // Global Settings
      // -----------------------------------
      $rootScope.app = {
        name: 'menuCLOUD',
        description: 'One Login for Everything Menu',
        year: ((new Date()).getFullYear()),
        layout: {
          isFixed: false,
          isCollapsed: true,
          isBoxed: false,
          isRTL: false,
          horizontal: false,
          isFloat: true,
          asideHover: false,
          theme: null,
          asideScrollbar: true
        },
        useFullLayout: false,
        hiddenFooter: false,
        offsidebarOpen: false,
        asideToggled: false,
        viewAnimation: 'ng-fadeIn'
      };

      // Setup the layout mode
      $rootScope.app.layout.horizontal = ( $rootScope.$stateParams.layout === 'app-h') ;

      // Restore layout settings [*** UNCOMMENT TO ENABLE ***]
      // if( angular.isDefined($localStorage.layout) )
      //   $rootScope.app.layout = $localStorage.layout;
      // else
      //   $localStorage.layout = $rootScope.app.layout;
      //
      // $rootScope.$watch('app.layout', function () {
      //   $localStorage.layout = $rootScope.app.layout;
      // }, true);

      // Close submenu when sidebar change from collapsed to normal
      $rootScope.$watch('app.layout.isCollapsed', function(newValue) {
        if( newValue === false )
          $rootScope.$broadcast('closeSidebarMenu');
      });

    }

})();

/**=========================================================
 * Component: sidebar-menu.js
 * Handle sidebar collapsible elements
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.sidebar')
        .controller('SidebarController', SidebarController);

    SidebarController.$inject = ['$rootScope', '$scope', '$state', 'SidebarLoader', 'Utils'];
    function SidebarController($rootScope, $scope, $state, SidebarLoader,  Utils) {

        activate();

        ////////////////

        function activate() {
          var collapseList = [];

          // demo: when switch from collapse to hover, close all items
          $rootScope.$watch('app.layout.asideHover', function(oldVal, newVal){
            if ( newVal === false && oldVal === true) {
              closeAllBut(-1);
            }
          });


          // Load menu from json file
          // ----------------------------------- 

          SidebarLoader.getMenu(sidebarReady);
          
          function sidebarReady(items) {
            $scope.menuItems = items;
            // console.log('menuItems: ', items)
          }

          // Handle sidebar and collapse items
          // ----------------------------------
          
          $scope.getMenuItemPropClasses = function(item) {
            return (item.heading ? 'nav-heading' : '') +
                   (isActive(item) ? ' active' : '') ;
          };

          $scope.addCollapse = function($index, item) {
            collapseList[$index] = $rootScope.app.layout.asideHover ? true : !isActive(item);
          };

          $scope.isCollapse = function($index) {
            return (collapseList[$index]);
          };

          $scope.toggleCollapse = function($index, isParentItem, needSideCollapse) {

            // collapsed sidebar doesn't toggle drodopwn
            if( Utils.isSidebarCollapsed() || $rootScope.app.layout.asideHover ) return true;

            // make sure the item index exists
            if( angular.isDefined( collapseList[$index] ) ) {
              if ( ! $scope.lastEventFromChild ) {
                collapseList[$index] = !collapseList[$index];
                closeAllBut($index);
              }
            }
            else if ( isParentItem ) {
              closeAllBut(-1);
            }
            
            $scope.lastEventFromChild = isChild($index);

            $rootScope.app.layout.isCollapsed = needSideCollapse;

            return true;
          
          };

          // Controller helpers
          // ----------------------------------- 

            // Check item and children active state
            function isActive(item) {

              if(!item) return;

              if( !item.sref || item.sref === '#') {
                var foundActive = false;
                angular.forEach(item.submenu, function(value) {
                  if(isActive(value)) foundActive = true;
                });
                return foundActive;
              }
              else
                return $state.is(item.sref) || $state.includes(item.sref);
            }

            function closeAllBut(index) {
              index += '';
              for(var i in collapseList) {
                if(index < 0 || index.indexOf(i) < 0)
                  collapseList[i] = true;
              }
            }

            function isChild($index) {
              /*jshint -W018*/
              return (typeof $index === 'string') && !($index.indexOf('-') < 0);
            }

          $scope.onPageGo = function (uri, title, needSideCollapse) {
            $rootScope.topnavbar.title = title;
            $rootScope.app.layout.isCollapsed = needSideCollapse;
            $state.go(uri);
          }
        } // activate
    }

})();

/**=========================================================
 * Component: sidebar.js
 * Wraps the sidebar and handles collapsed state
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.sidebar')
        .directive('sidebar', sidebar);

    sidebar.$inject = ['$rootScope', '$timeout', '$window', 'Utils'];
    function sidebar ($rootScope, $timeout, $window, Utils) {
        var $win = angular.element($window);
        var directive = {
            // bindToController: true,
            // controller: Controller,
            // controllerAs: 'vm',
            link: link,
            restrict: 'EA',
            template: '<nav class="sidebar" ng-transclude></nav>',
            transclude: true,
            replace: true
            // scope: {}
        };
        return directive;

        function link(scope, element, attrs) {

          var currentState = $rootScope.$state.current.name;
          var $sidebar = element;

          var eventName = Utils.isTouch() ? 'click' : 'mouseenter' ;
          var subNav = $();

          $sidebar.on( eventName, '.nav > li', function() {

            if( Utils.isSidebarCollapsed() || $rootScope.app.layout.asideHover ) {

              subNav.trigger('mouseleave');
              subNav = toggleMenuItem( $(this), $sidebar);

              // Used to detect click and touch events outside the sidebar          
              sidebarAddBackdrop();

            }

          });

          scope.$on('closeSidebarMenu', function() {
            removeFloatingNav();
          });

          // Normalize state when resize to mobile
          $win.on('resize', function() {
            if( ! Utils.isMobile() )
          	asideToggleOff();
          });

          // Adjustment on route changes
          $rootScope.$on('$stateChangeStart', function(event, toState) {
            currentState = toState.name;
            // Hide sidebar automatically on mobile
            asideToggleOff();

            $rootScope.$broadcast('closeSidebarMenu');
          });

      	  // Autoclose when click outside the sidebar
          if ( angular.isDefined(attrs.sidebarAnyclickClose) ) {
            
            var wrapper = $('.wrapper');
            var sbclickEvent = 'click.sidebar';
            
            $rootScope.$watch('app.asideToggled', watchExternalClicks);

          }

          //////

          function watchExternalClicks(newVal) {
            // if sidebar becomes visible
            if ( newVal === true ) {
              $timeout(function(){ // render after current digest cycle
                wrapper.on(sbclickEvent, function(e){
                  // if not child of sidebar
                  if( ! $(e.target).parents('.aside').length ) {
                    asideToggleOff();
                  }
                });
              });
            }
            else {
              // dettach event
              wrapper.off(sbclickEvent);
            }
          }

          function asideToggleOff() {
            $rootScope.app.asideToggled = false;
            if(!scope.$$phase) scope.$apply(); // anti-pattern but sometimes necessary
      	  }
        }
        
        ///////

        function sidebarAddBackdrop() {
          var $backdrop = $('<div/>', { 'class': 'dropdown-backdrop'} );
          $backdrop.insertAfter('.aside-inner').on('click mouseenter', function () {
            removeFloatingNav();
          });
        }

        // Open the collapse sidebar submenu items when on touch devices 
        // - desktop only opens on hover
        function toggleTouchItem($element){
          $element
            .siblings('li')
            .removeClass('open')
            .end()
            .toggleClass('open');
        }

        // Handles hover to open items under collapsed menu
        // ----------------------------------- 
        function toggleMenuItem($listItem, $sidebar) {

          removeFloatingNav();

          var ul = $listItem.children('ul');
          
          if( !ul.length ) return $();
          if( $listItem.hasClass('open') ) {
            toggleTouchItem($listItem);
            return $();
          }

          var $aside = $('.aside');
          var $asideInner = $('.aside-inner'); // for top offset calculation
          // float aside uses extra padding on aside
          var mar = parseInt( $asideInner.css('padding-top'), 0) + parseInt( $aside.css('padding-top'), 0);
          var subNav = ul.clone().appendTo( $aside );
          
          toggleTouchItem($listItem);

          var itemTop = ($listItem.position().top + mar) - $sidebar.scrollTop();
          var vwHeight = $win.height();

          subNav
            .addClass('nav-floating')
            .css({
              position: $rootScope.app.layout.isFixed ? 'fixed' : 'absolute',
              top:      itemTop,
              bottom:   (subNav.outerHeight(true) + itemTop > vwHeight) ? 0 : 'auto'
            });

          subNav.on('mouseleave', function() {
            toggleTouchItem($listItem);
            subNav.remove();
          });

          return subNav;
        }

        function removeFloatingNav() {
          $('.dropdown-backdrop').remove();
          $('.sidebar-subnav.nav-floating').remove();
          $('.sidebar li.open').removeClass('open');
        }
    }


})();


(function() {
    'use strict';

    angular
        .module('app.sidebar')
        .run(sidebarRun);

    sidebarRun.$inject = ['$rootScope', '$state', '$stateParams',  '$window', '$templateCache', 'Colors', 'AuthService','$localStorage', '$http'];
    
    function sidebarRun($rootScope, $state, $stateParams, $window, $templateCache, Colors, AuthService, $localStorage, $http) {
        $rootScope.sidebar = {
            quickLinks: [
                { sref: 'app.restaurant', text: 'First restaurant' },
                { sref: 'app.restaurant', text: 'Restaurant Two' }
            ]
        };
    }

})();


(function() {
    'use strict';

    angular
        .module('app.sidebar')
        .service('SidebarLoader', SidebarLoader);

    SidebarLoader.$inject = ['$http'];
    function SidebarLoader($http) {
        this.getMenu = getMenu;

        ////////////////

        function getMenu(onReady, onError) {
          var menuJson = 'server/sidebar-menu.json',
              menuURL  = menuJson + '?v=' + (new Date().getTime()); // jumps cache
            
          onError = onError || function() { alert('Failure loading menu'); };

          $http
            .get(menuURL)
            .success(onReady)
            .error(onError);
        }
    }
})();
(function() {
    'use strict';

    angular
        .module('app.sidebar')
        .controller('UserBlockController', UserBlockController);

    UserBlockController.$inject = ['$rootScope', '$scope'];
    function UserBlockController($rootScope, $scope) {

        activate();

        ////////////////

        function activate() {
          // $rootScope.user = {
          //   name:     'John',
          //   job:      'ng-developer',
          //   picture:  'app/img/user/02.jpg'
          // };

          // Hides/show user avatar on sidebar
          $rootScope.toggleUserBlock = function(){
            $rootScope.$broadcast('toggleUserBlock');
          };

          $rootScope.userBlockVisible = false;

          var detach = $rootScope.$on('toggleUserBlock', function(/*event, args*/) {

            $rootScope.userBlockVisible = ! $rootScope.userBlockVisible;

          });

          $scope.$on('$destroy', detach);
        }
    }
})();

(function() {
    'use strict';

    angular
        .module('app.subtopnavbar')
        .config(subtopnavbarConfig);

    subtopnavbarConfig.$inject = ['$controllerProvider', '$compileProvider', '$filterProvider', '$provide', '$animateProvider'];
    function subtopnavbarConfig($controllerProvider, $compileProvider, $filterProvider, $provide, $animateProvider){

        var subtopnavbar = angular.module('app.subtopnavbar');
        // registering components after bootstrap
        subtopnavbar.controller = $controllerProvider.register;
        subtopnavbar.directive  = $compileProvider.directive;
        subtopnavbar.filter     = $filterProvider.register;
        subtopnavbar.factory    = $provide.factory;
        subtopnavbar.service    = $provide.service;
        subtopnavbar.constant   = $provide.constant;
        subtopnavbar.value      = $provide.value;
        // Disables animation on items with class .ng-no-animation

        $animateProvider.classNameFilter(/^((?!(ng-no-animation)).)*$/);

    }

})();
/**=========================================================
 * Component: business-list.controller.js
 *
 =========================================================*/

(function () {
  'use strict';

  angular
    .module('app.subtopnavbar')
    .controller('SubtopnavbarCtrl', SubtopnavbarCtrl)

  SubtopnavbarCtrl.$inject = [
    '$rootScope'
    , '$scope'
    , '$state'
  ];

  function SubtopnavbarCtrl($rootScope, $scope, $state) {
    var vm = this;

    activate();

    ////////////////

    function activate() {
      $rootScope.topnavbar.title = 'FIRST RESTAURANT';
      $rootScope.subtopnavbar.tabId = 0;
      $rootScope.subtopnavbar.navBack = {
        uri: '#'
      };

      $scope.onSelectTab = function (idx) {
        $rootScope.subtopnavbar.tabId = idx;
        console.log('selected tab idx: ', $rootScope.subtopnavbar.tabId);
      }
    }
  }

})();

(function() {
    'use strict';

    angular
        .module('app.subtopnavbar')
        .run(subtopnavbarRun);

    subtopnavbarRun.$inject = ['$rootScope', '$state', '$stateParams',  '$window', '$templateCache', 'Colors', 'AuthService','$localStorage', '$http'];
    
    function subtopnavbarRun($rootScope, $state, $stateParams, $window, $templateCache, Colors, AuthService, $localStorage, $http) {
        $rootScope.subtopnavbar = {
            tabId: 0
        };
    }

})();


(function() {
    'use strict';

    angular
        .module('app.subtopnavbar')
        .service('SubtopnavbarService', SubtopnavbarService);

    SubtopnavbarService.$inject = ['$http', '$q', '$localStorage', '$httpParamSerializer', '$rootScope'];
    
    function SubtopnavbarService($http, $q, $localStorage, $httpParamSerializer, $rootScope) {
    }
    
})();
(function() {
    'use strict';

    angular
        .module('app.toaster')
        .service('ToasterService', ToasterService);

    ToasterService.$inject = ['toaster'];
    function ToasterService(toaster) {
      
      function popToast(data){
        if(!data.timeout) data.timeout = 5000
        if(!_.isString(data.body)){
          if(_.isObject(data.body)){
            if(data.body.invalidAttributes){
              var errMsg = ''
              _.each(data.body.invalidAttributes, function(value, key, list){
                _.each(value, function(message, key, list){
                  if(message && _.isString(message)) errMsg += message+"\n"
                });
              });
              if(errMsg) data.body = errMsg
            }else if(data.body.message){
              data.body = data.body.message
            }else if(data.body.error){
              data.body = data.body.error
            }
          }
        }
        toaster.pop(data)
      }
      function toastError(title,body,options){
        var data = {
          type: 'error'
          ,title: title || "Error"
          ,body: body || "Something went wrong"
        }
        if(options && _.isObject(options)) data = _.extend(_.omit(data, 'type', 'title', 'body'), options);
        popToast(data)
      }
      function toastSuccess(title,body,options){
        var data = {
          type: 'success'
          ,title: title || "Success"
          ,body: body || "Changed has been saved"
        }
        if(options && _.isObject(options)) data = _.extend(_.omit(data, 'type', 'title', 'body'), options);
        popToast(data)
      }
      function toastWarning(title,body,options){
        var data = {
          type: 'warning'
          ,title: title || "Warning"
          ,body: body || "Something important just happened"
        }
        if(options && _.isObject(options)) data = _.extend(_.omit(data, 'type', 'title', 'body'), options);
        popToast(data)
      }
      
      function toastInfo(title,body,options){
        var data = {
          type: 'info'
          ,title: title || "Info"
          ,body: body || "You need to know something"
        }
        if(options && _.isObject(options)) data = _.extend(_.omit(data, 'type', 'title', 'body'), options);
        popToast(data)
      }

      return {
        error: toastError
        ,success: toastSuccess
        ,warning: toastWarning
        ,info: toastInfo
      }
    }
})();
(function() {
    'use strict';

    angular
        .module('app.translate')
        .config(translateConfig)
        ;
    translateConfig.$inject = ['$translateProvider'];
    function translateConfig($translateProvider){

      $translateProvider.useStaticFilesLoader({
          prefix : 'app/i18n/',
          suffix : '.json'
      });

      $translateProvider.preferredLanguage('en');
      $translateProvider.useLocalStorage();
      $translateProvider.usePostCompiling(true);
      $translateProvider.useSanitizeValueStrategy('sanitizeParameters');

      $translateProvider.translations('en',{
          DIALOGS_ERROR: "Error",
          DIALOGS_ERROR_MSG: "An unknown error has occurred.",
          DIALOGS_CLOSE: "Close",
          DIALOGS_PLEASE_WAIT: "Please Wait",
          DIALOGS_PLEASE_WAIT_ELIPS: "Please Wait...",
          DIALOGS_PLEASE_WAIT_MSG: "Waiting on operation to complete.",
          DIALOGS_PERCENT_COMPLETE: "% Complete",
          DIALOGS_NOTIFICATION: "Notification",
          DIALOGS_NOTIFICATION_MSG: "Unknown application notification.",
          DIALOGS_CONFIRMATION: "Confirmation",
          DIALOGS_CONFIRMATION_MSG: "Confirmation required.",
          DIALOGS_OK: "OK",
          DIALOGS_YES: "Yes",
          DIALOGS_NO: "No"
      });
    }
})();
(function() {
    'use strict';

    angular
        .module('app.translate')
        .run(translateRun)
        ;
    translateRun.$inject = ['$rootScope', '$translate'];
    
    function translateRun($rootScope, $translate){

      // Internationalization
      // ----------------------

      $rootScope.language = {
        // Handles language dropdown
        listIsOpen: false,
        // list of available languages
        available: {
          'en':       'English',
          // 'en_US':       'English US',
          'es_AR':    'Español'
        },
        // display always the current ui language
        init: function () {
          var proposedLanguage = $translate.proposedLanguage() || $translate.use();
          var preferredLanguage = $translate.preferredLanguage(); // we know we have set a preferred one in app.config
          $rootScope.language.selected = $rootScope.language.available[ (proposedLanguage || preferredLanguage) ];
        },
        set: function (localeId) {
          // Set the new idiom
          $translate.use(localeId);
          // save a reference for the current language
          $rootScope.language.selected = $rootScope.language.available[localeId];
          // finally toggle dropdown
          $rootScope.language.listIsOpen = ! $rootScope.language.listIsOpen;
        }
      };

      $rootScope.language.init();

    }
})();
/**=========================================================
 * Component: animate-enabled.js
 * Enable or disables ngAnimate for element with directive
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.utils')
        .directive('animateEnabled', animateEnabled);

    animateEnabled.$inject = ['$animate'];
    function animateEnabled ($animate) {
        var directive = {
            link: link,
            restrict: 'A'
        };
        return directive;

        function link(scope, element, attrs) {
          scope.$watch(function () {
            return scope.$eval(attrs.animateEnabled, scope);
          }, function (newValue) {
            $animate.enabled(!!newValue, element);
          });
        }
    }

})();

/**=========================================================
 * Component: browser.js
 * Browser detection
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.utils')
        .service('Browser', Browser);

    Browser.$inject = ['$window'];
    function Browser($window) {
      return $window.jQBrowser;
    }

})();

/**=========================================================
 * Component: clear-storage.js
 * Removes a key from the browser storage via element click
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.utils')
        .directive('resetKey', resetKey);

    resetKey.$inject = ['$state', '$localStorage'];
    function resetKey ($state, $localStorage) {
        var directive = {
            link: link,
            restrict: 'A',
            scope: {
              resetKey: '@'
            }
        };
        return directive;

        function link(scope, element) {
          element.on('click', function (e) {
              e.preventDefault();

              if(scope.resetKey) {
                delete $localStorage[scope.resetKey];
                $state.go($state.current, {}, {reload: true});
              }
              else {
                $.error('No storage key specified for reset.');
              }
          });
        }
    }

})();

/**=========================================================
 * Component: fullscreen.js
 * Toggle the fullscreen mode on/off
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.utils')
        .directive('toggleFullscreen', toggleFullscreen);

    toggleFullscreen.$inject = ['Browser'];
    function toggleFullscreen (Browser) {
        var directive = {
            link: link,
            restrict: 'A'
        };
        return directive;

        function link(scope, element) {
          // Not supported under IE
          if( Browser.msie ) {
            element.addClass('hide');
          }
          else {
            element.on('click', function (e) {
                e.preventDefault();

                if (screenfull.enabled) {
                  
                  screenfull.toggle();
                  
                  // Switch icon indicator
                  if(screenfull.isFullscreen)
                    $(this).children('em').removeClass('fa-expand').addClass('fa-compress');
                  else
                    $(this).children('em').removeClass('fa-compress').addClass('fa-expand');

                } else {
                  $.error('Fullscreen not enabled');
                }

            });
          }
        }
    }


})();

/**=========================================================
 * Component: load-css.js
 * Request and load into the current page a css file
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.utils')
        .directive('loadCss', loadCss);

    function loadCss () {
        var directive = {
            link: link,
            restrict: 'A'
        };
        return directive;

        function link(scope, element, attrs) {
          element.on('click', function (e) {
              if(element.is('a')) e.preventDefault();
              var uri = attrs.loadCss,
                  link;

              if(uri) {
                link = createLink(uri);
                if ( !link ) {
                  $.error('Error creating stylesheet link element.');
                }
              }
              else {
                $.error('No stylesheet location defined.');
              }

          });
        }
        
        function createLink(uri) {
          var linkId = 'autoloaded-stylesheet',
              oldLink = $('#'+linkId).attr('id', linkId + '-old');

          $('head').append($('<link/>').attr({
            'id':   linkId,
            'rel':  'stylesheet',
            'href': uri
          }));

          if( oldLink.length ) {
            oldLink.remove();
          }

          return $('#'+linkId);
        }
    }

})();

/**=========================================================
 * Component: now.js
 * Provides a simple way to display the current time formatted
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.utils')
        .directive('now', now);

    now.$inject = ['dateFilter', '$interval'];
    function now (dateFilter, $interval) {
        var directive = {
            link: link,
            restrict: 'EA'
        };
        return directive;

        function link(scope, element, attrs) {
          var format = attrs.format;

          function updateTime() {
            var dt = dateFilter(new Date(), format);
            element.text(dt);
          }

          updateTime();
          var intervalPromise = $interval(updateTime, 1000);

          scope.$on('$destroy', function(){
            $interval.cancel(intervalPromise);
          });

        }
    }

})();

/**=========================================================
 * Component: table-checkall.js
 * Tables check all checkbox
 =========================================================*/
(function() {
    'use strict';

    angular
        .module('app.utils')
        .directive('checkAll', checkAll);

    function checkAll () {
        var directive = {
            link: link,
            restrict: 'A'
        };
        return directive;

        function link(scope, element) {
          element.on('change', function() {
            var $this = $(this),
                index= $this.index() + 1,
                checkbox = $this.find('input[type="checkbox"]'),
                table = $this.parents('table');
            // Make sure to affect only the correct checkbox column
            table.find('tbody > tr > td:nth-child('+index+') input[type="checkbox"]')
              .prop('checked', checkbox[0].checked);

          });
        }
    }

})();

/**=========================================================
 * Component: trigger-resize.js
 * Triggers a window resize event from any element
 =========================================================*/
(function() {
    'use strict';

    angular
        .module('app.utils')
        .directive('triggerResize', triggerResize);

    triggerResize.$inject = ['$window', '$timeout'];
    function triggerResize ($window, $timeout) {
        var directive = {
            link: link,
            restrict: 'A'
        };
        return directive;

        function link(scope, element, attributes) {
          element.on('click', function(){
            $timeout(function(){
              // all IE friendly dispatchEvent
              var evt = document.createEvent('UIEvents');
              evt.initUIEvent('resize', true, false, $window, 0);
              $window.dispatchEvent(evt);
              // modern dispatchEvent way
              // $window.dispatchEvent(new Event('resize'));
            }, attributes.triggerResize || 300);
          });
        }
    }

})();

/**=========================================================
 * Component: utils.js
 * Utility library to use across the theme
 =========================================================*/

(function() {
    'use strict';

    angular
        .module('app.utils')
        .service('Utils', Utils);

    Utils.$inject = ['$window', 'APP_MEDIAQUERY'];
    function Utils($window, APP_MEDIAQUERY) {

        var $html = angular.element('html'),
            $win  = angular.element($window),
            $body = angular.element('body');

        return {
          // DETECTION
          support: {
            transition: (function() {
                    var transitionEnd = (function() {

                        var element = document.body || document.documentElement,
                            transEndEventNames = {
                                WebkitTransition: 'webkitTransitionEnd',
                                MozTransition: 'transitionend',
                                OTransition: 'oTransitionEnd otransitionend',
                                transition: 'transitionend'
                            }, name;

                        for (name in transEndEventNames) {
                            if (element.style[name] !== undefined) return transEndEventNames[name];
                        }
                    }());

                    return transitionEnd && { end: transitionEnd };
                })(),
            animation: (function() {

                var animationEnd = (function() {

                    var element = document.body || document.documentElement,
                        animEndEventNames = {
                            WebkitAnimation: 'webkitAnimationEnd',
                            MozAnimation: 'animationend',
                            OAnimation: 'oAnimationEnd oanimationend',
                            animation: 'animationend'
                        }, name;

                    for (name in animEndEventNames) {
                        if (element.style[name] !== undefined) return animEndEventNames[name];
                    }
                }());

                return animationEnd && { end: animationEnd };
            })(),
            requestAnimationFrame: window.requestAnimationFrame ||
                                   window.webkitRequestAnimationFrame ||
                                   window.mozRequestAnimationFrame ||
                                   window.msRequestAnimationFrame ||
                                   window.oRequestAnimationFrame ||
                                   function(callback){ window.setTimeout(callback, 1000/60); },
            /*jshint -W069*/
            touch: (
                ('ontouchstart' in window && navigator.userAgent.toLowerCase().match(/mobile|tablet/)) ||
                (window.DocumentTouch && document instanceof window.DocumentTouch)  ||
                (window.navigator['msPointerEnabled'] && window.navigator['msMaxTouchPoints'] > 0) || //IE 10
                (window.navigator['pointerEnabled'] && window.navigator['maxTouchPoints'] > 0) || //IE >=11
                false
            ),
            mutationobserver: (window.MutationObserver || window.WebKitMutationObserver || window.MozMutationObserver || null)
          },
          // UTILITIES
          isInView: function(element, options) {
              /*jshint -W106*/
              var $element = $(element);

              if (!$element.is(':visible')) {
                  return false;
              }

              var window_left = $win.scrollLeft(),
                  window_top  = $win.scrollTop(),
                  offset      = $element.offset(),
                  left        = offset.left,
                  top         = offset.top;

              options = $.extend({topoffset:0, leftoffset:0}, options);

              if (top + $element.height() >= window_top && top - options.topoffset <= window_top + $win.height() &&
                  left + $element.width() >= window_left && left - options.leftoffset <= window_left + $win.width()) {
                return true;
              } else {
                return false;
              }
          },
          
          langdirection: $html.attr('dir') === 'rtl' ? 'right' : 'left',

          isTouch: function () {
            return $html.hasClass('touch');
          },

          isSidebarCollapsed: function () {
            return $body.hasClass('aside-collapsed');
          },

          isSidebarToggled: function () {
            return $body.hasClass('aside-toggled');
          },

          isMobile: function () {
            return $win.width() < APP_MEDIAQUERY.tablet;
          }

        };
    }
})();

(function() {
    'use strict';

    angular
        .module('menucloud.custom', []);
})();
(function () {
  'use strict';

  angular
    .module('app.dashboard', []);
})();
(function() {
  'use strict';

  angular
    .module('menucloud.services', []);
})();
(function () {
  'use strict';

  angular
    .module('app.restaurant', [
      'ngAutocomplete'
    ]);
})();
(function() {
    'use strict';

    angular
        .module('menucloud.custom')
        .config(Config)
        
    Config.$inject = [
      /*'$controllerProvider'
      ,'$compileProvider'
      ,'$filterProvider'
      ,'$provide'
      ,'$animateProvider'
      ,'$locationProvider'
      ,'$urlRouterProvider'
      ,*/
    ];

    function Config(
      /*$controllerProvider
      ,$compileProvider
      ,$filterProvider
      ,$provide
      ,$animateProvider
      ,$locationProvider
      ,$urlRouterProvider
      ,*/
    ){
      
      /*
      var menu = angular.module('menu');
      // registering components after bootstrap
      menu.controller = $controllerProvider.register;
      menu.directive  = $compileProvider.directive;
      menu.filter     = $filterProvider.register;
      menu.factory    = $provide.factory;
      // menu.service    = $provide.service;
      menu.constant   = $provide.constant;
      menu.value      = $provide.value;
      
      $animateProvider.classNameFilter(/^((?!(ng-no-animation)).)*$/);
      */

    }

})();
/**=========================================================
 * Component: business-list.controller.js
 *
 =========================================================*/

(function() {
    'use strict';

    angular
      .module('menucloud.custom')
      .controller('CustomElementCtrl', CustomElementCtrl)

    CustomElementCtrl.$inject = [
      '$rootScope'
      ,'$scope'
      ,'$state'
      ,'$stateParams'
      ,'dialogs'
      ,'$moment'
    ];
    
    function CustomElementCtrl($rootScope, $scope, $state, $stateParams, dialogs, $moment) {
      var vm = this;

      
      activate();

      ////////////////
      
      function activate() {
        /*SomeService.find().then(
          function (res){
            vm.element res.data
            //success
            console.log('data: ', res.data)
          },
          function (res){
            // fail
            console.log('error: ', res.data)
          }
        )*/
      }
    }
    
})();

/**=========================================================
 * Component: business-list.controller.js
 *
 =========================================================*/

(function() {
    'use strict';

    angular
      .module('menucloud.custom')
      .controller('CustomListCtrl', CustomListCtrl)

    CustomListCtrl.$inject = [
      '$rootScope'
      ,'$scope'
      ,'$state'
      ,'$stateParams'
      ,'dialogs'
      ,'$moment'
    ];
    
    function CustomListCtrl($rootScope, $scope, $state, $stateParams, dialogs, $moment) {
      var vm = this;
      vm.errMsg = '';
      vm.title = 'Custom List'
      
      
      ////////////////
      
      function activate() {
        /**/
        /*event listeners*/
        
      }
    }
    
})();

(function() {
    'use strict';

    angular
        .module('menucloud.custom')
        .config(Config)
        
    Config.$inject = [
      '$stateProvider'
      ,'RouteHelpersProvider'
      ,'$urlRouterProvider'
    ];

    function Config(
      $stateProvider
      ,helper
      ,$urlRouterProvider
    ){
      // @NOTE: this is guide template router. views does not exists
      /*
      $stateProvider
        .state('app.custom', {
          url: '/custom',
          title: "Custom List",
          templateUrl: helper.basepath('custom/custom.list.html')
        })
        .state('app.custom-element',{
          url: '/custom/:menu_id',
          title: "Custom Element"

          controller: "CustomElementCtrl",
          controllerAs: "vm",
          templateUrl: helper.basepath('custom/custom.element.html')
        })
      */
    }

})();
(function () {
  'use strict';

  angular
    .module('app.dashboard')
    .config(dashboardConfig);

  dashboardConfig.$inject = ['$controllerProvider', '$compileProvider', '$filterProvider', '$provide', '$animateProvider'];
  function dashboardConfig($controllerProvider, $compileProvider, $filterProvider, $provide, $animateProvider) {

    var dashboard = angular.module('app.dashboard');
    // registering components after bootstrap
    dashboard.controller = $controllerProvider.register;
    dashboard.directive = $compileProvider.directive;
    dashboard.filter = $filterProvider.register;
    dashboard.factory = $provide.factory;
    dashboard.service = $provide.service;
    dashboard.constant = $provide.constant;
    dashboard.value = $provide.value;
    // Disables animation on items with class .ng-no-animation

    $animateProvider.classNameFilter(/^((?!(ng-no-animation)).)*$/);

  }

})();
/**=========================================================
 * Component: business-list.controller.js
 *
 =========================================================*/

(function () {
  'use strict';

  angular
    .module('app.dashboard')
    .controller('DashboardCtrl', DashboardCtrl)

  DashboardCtrl.$inject = [
    '$rootScope'
    , '$scope'
    , '$state'
  ];

  function DashboardCtrl($rootScope, $scope, $state) {
    var vm = this;

    activate();

    ////////////////

    function activate() {
      $rootScope.topnavbar.title = 'MENU OVERVIEW';
      $rootScope.app.layout.hasSubTopNavBar = false;
    }
  }

})();

(function () {
  'use strict';

  angular
    .module('app.dashboard')
    .run(dashboardRun);

  dashboardRun.$inject = ['$rootScope'];

  function dashboardRun($rootScope) {
    $rootScope.topnavbar = {
      title: 'MENU OVERVIEW'
    };
  }

})();


(function () {
  'use strict';

  angular
    .module('app.dashboard')
    .service('DashboardService', DashboardService);

  DashboardService.$inject = ['$http', '$q', '$localStorage', '$httpParamSerializer', '$rootScope'];

  function DashboardService($http, $q, $localStorage, $httpParamSerializer, $rootScope) {
  }

})();
(function() {
    'use strict';

    angular
        .module('menucloud.services')
        .service('BusinessService', BusinessService);

    BusinessService.$inject = [
      '$http'
      ,'$q'
      ,'$localStorage'
      ,'$httpParamSerializer'
      ,'$rootScope'
      ,'RestFactory'
      ,'$sails'
      /*,'MenuService'*/
    ];
    
    function BusinessService($http, $q, $localStorage, $httpParamSerializer, $rootScope, RestFactory, $sails/*, MenuService*/) {

        
        var model_name = 'business'
        var Service = function(){
          RestFactory.apply(this,arguments)
        }
        var Serviceinstance = new Service(model_name)
        /*********************************************************************************
        @GUIDE:
        */
        // custom method example (see ../components/factories/rest.factory for details)
        /*
        Serviceinstance.yourAction = yourAction

        function yourAction(id, body){
          var action = '/action/'+id
          
          // arguments: (method, action, body)
          // @method: 'get' || 'put' || 'post' || 'delete'
          // @action: '/someaction'
          // @body: Object @optional
          
          return Serviceinstance.customAction('put', action, body)
        }
        **********************************************************************************/
        
        return  Serviceinstance;
        
    }
})();
(function() {
    'use strict';

    angular
        .module('menucloud.services')
        .service('InviteService', InviteService);

    InviteService.$inject = [
      '$http'
      ,'$q'
      ,'$localStorage'
      ,'$httpParamSerializer'
      ,'$rootScope'
      ,'RestFactory'
      ,'$sails'
      /*,'MenuService'*/
    ];
    
    function InviteService($http, $q, $localStorage, $httpParamSerializer, $rootScope, RestFactory, $sails/*, MenuService*/) {

        var model_name = 'invite'
        var Service = function(){
          RestFactory.apply(this,arguments)
        }
        var Serviceinstance = new Service(model_name)
        // custom method example (see ../components/factories/rest.factory for details)
        /*
        Serviceinstance.yourAction = yourAction

        function yourAction(id, body){
          var action = '/action/'+id
          
          // arguments: (method, action, body)
          // @method: 'get' || 'put' || 'post' || 'delete'
          // @action: '/someaction'
          // @body: Object @optional
          
          return Serviceinstance.customAction('put', action, body)
        }
        */
        Serviceinstance.resend = resend
        Serviceinstance.accept = accept
        
        function accept(id, body){
          var action = '/accept/'+id
          return Serviceinstance.customAction('put', action, body)
        }

        function resend(id){
          var action = '/resend/'+id
          return Serviceinstance.customAction('get', action)
        };
        return  Serviceinstance;
    }
})();
(function() {
    'use strict';

    angular
        .module('menucloud.services')
        .service('MenuService', MenuService);

    MenuService.$inject = [
      '$http'
      ,'$q'
      ,'$localStorage'
      ,'$httpParamSerializer'
      ,'$rootScope'
      ,'RestFactory'
      ,'$sails'
      /*,'MenuService'*/
    ];
    
    function MenuService($http, $q, $localStorage, $httpParamSerializer, $rootScope, RestFactory, $sails/*, MenuService*/) {

        
        var model_name = 'menu'
        var Service = function(){
          RestFactory.apply(this,arguments)
        }
        var Serviceinstance = new Service(model_name)
        
        /*********************************************************************************
        @GUIDE:
        */
        // custom method example (see ../components/factories/rest.factory for details)
        /*
        Serviceinstance.yourAction = yourAction

        function yourAction(id, body){
          var action = '/action/'+id
          
          // arguments: (method, action, body)
          // @method: 'get' || 'put' || 'post' || 'delete'
          // @action: '/someaction'
          // @body: Object @optional
          
          return Serviceinstance.customAction('put', action, body)
        }
        **********************************************************************************/
        return  Serviceinstance;

        /*
        // @NOTE: commented out cause I'm not sure if we will need these methods here
        Service.prototype.removeSource = function (id,file_id) {
          var that = this
          var deferred = $q.defer();
          var path = that.endpoint + '/'+id+'/sources/'+file_id
          var promise
          if(that.sails_on){
            promise = $sails.delete(path).then(deferred.resolve, deferred.reject)
          }else{
            promise = $http.delete(path).then(deferred.resolve, deferred.reject)
          }
          return deferred.promise;
        }

        Service.prototype.completeMenu = completeMenu
        function completeMenu(id){
          var that = this
          var deferred = $q.defer();
          var path = that.endpoint + '/'+id+'/completeMenu'
          var promise
          if(that.sails_on){
            promise = $sails.put(path).then(deferred.resolve, deferred.reject)
          }else{
            promise = $http.put(path).then(deferred.resolve, deferred.reject)
          }
          return deferred.promise;
        }
        Service.prototype.updateItems = updateItems
        
        function updateItems(body){
          var that = this
          var deferred = $q.defer();
          var path = that.endpoint + '/'+body.id+'/items'
          var promise
          if(that.sails_on){
            promise = $sails.put(path, body).then(deferred.resolve, deferred.reject)
          }else{
            promise = $http.put(path, body).then(deferred.resolve, deferred.reject)
          }
          return deferred.promise;
        }
        
        Service.prototype.populateItems = populateItems
        function populateItems(id){
          var that = this
          var deferred = $q.defer();
          var path = that.endpoint + '/'+id+'/items'
          var promise
          if(that.sails_on){
            promise = $sails.get(path).then(deferred.resolve, deferred.reject)
          }else{
            promise = $http.get(path).then(deferred.resolve, deferred.reject)
          }
          return deferred.promise;
        }
        
        */
    }
})();
/**=========================================================
 * Component: business-list.controller.js
 *
 =========================================================*/

(function () {
  'use strict';

  angular
    .module('app.restaurant')
    .controller('RestaurantOverviewCtrl', RestaurantOverviewCtrl)

  RestaurantOverviewCtrl.$inject = [
    '$rootScope'
    , '$scope'
    , '$state'
    , 'BusinessService'
    , '$timeout'
  ];

  function RestaurantOverviewCtrl($rootScope, $scope, $state, BusinessService, $timeout) {
    var vm = this;

    vm.updatePlaceData = updatePlaceData
    /*
     GOOGLE PLACES scope
     auto complete
     */
    vm.ac_search = '' //searchfield
    vm.ac_details = null // google place data will be here
    vm.ac_options = { // options to obtain place details
      type: 'geocode',
      types: 'establishment',
      // bounds: {SWLat: 49, SWLng: -97, NELat: 50, NELng: -96},
      country: 'ca',
      language: 'eng',
      typesEnabled: false,
      boundsEnabled: false,
      componentEnabled: false,
      watchEnter: true
    };
    $rootScope.$watch('vm.ac_details', function (data) {
      if (data) {
        vm.newRestaurant.place_data = {}
        console.log('place details: ', data)
        var addr = {
          line_1: "",
          line_2: "",
          city: "",
          region: "",
          country: "",
          postal_code: "",
          place_id: data.place_id
        }
        _.each(data.address_components, function (el, key, list) {
          if (el.types) {
            if (el.types.indexOf("route") != -1) addr.line_1 = addr.line_1 + " " + el.long_name
            if (el.types.indexOf("street_number") != -1) addr.line_1 = el.long_name + addr.line_1
            if (el.types.indexOf("administrative_area_level_1") != -1) addr.region += el.long_name
            if (el.types.indexOf("country") != -1) addr.country += el.long_name
            if (el.types.indexOf("postal_code") != -1) addr.postal_code += el.long_name
            // sometimes city comes as sublocality and sometimes as locality
            if (el.types.indexOf("sublocality") != -1 && el.types.indexOf("sublocality_level_1") != -1) addr.city = el.long_name
            if (el.types.indexOf("locality") != -1 && el.types.indexOf("political") != -1) addr.city = el.long_name
          }
        });
        vm.newRestaurant.address = addr
        vm.updatePlaceData(data)
      }
    })


    function updatePlaceData(data) {
      console.log('updatePlaceData: ')
      if (data) {
        var merge_data = {
          opening_hours: _.omit(data.opening_hours, 'open_now')
          , place_id: data.place_id
          , website: data.website
          , name: data.name
          , phone: data.formatted_phone_number
          , formatted_address: data.formatted_address
          , international_phone_number: data.international_phone_number
        }

        if (data.geometry && data.geometry.location) {
          if (data.geometry.location.lat) merge_data.lat = data.geometry.location.lat()
          if (data.geometry.location.lng) merge_data.lng = data.geometry.location.lng()
        }
        vm.newRestaurant.place_data = _.extend(vm.newRestaurant.place_data, merge_data);
      }
    }

    activate();

    ////////////////

    function activate() {
      $rootScope.topnavbar.title = 'RESTAURANTS';
      $rootScope.app.layout.hasSubTopNavBar = false;
      vm.newRestaurant = {
        place_data: {},
        address: {},
        email: '',
        phone: ''
      };
      $scope.addRestaurant = function () {
        var htmlText =
          "<form class='form-wrapper' ng-controller='RestaurantOverviewCtrl as vm'>" +
          "<div class='form-item'>" +
          "<input type='text' ng-model='' placeholder='Name'>" +
          "</div>" +
          "<div class='form-item icon-right icon-location-pin2'>" +
          "<input id='locationInput' type='text' placeholder='Find location' name='place_data' ng-model='vm.ac_search' ng-autocomplete='' options='vm.ac_options' details='vm.ac_details'>" +
          "</div>" +
          "<div class='form-item'>" +
          "<input type='text' ng-model='' placeholder='Address'>" +
          "</div>" +
          "<div class='form-item'>" +
          "<input type='email' ng-model='' placeholder='Mail'>" +
          "</div>" +
          "<div class='form-item'>" +
          "<input type='number' ng-model='' placeholder='Phone'>" +
          "</div>" +
          "</form>";

        swal({
          title: "ADD A NEW RESTAURANT TO MENU CLOUD",
          text: htmlText,
          imageUrl: null,
          customClass: "app-alert has-close-button form-alert no-fieldset",
          showCancelButton: true,
          confirmButtonColor: "#5bc0de",
          confirmButtonText: "ADD",
          cancelButtonText: "",
          allowOutsideClick: true,
          html: true
        }, function () {
          console.log('click add restaurant New Restaurant: ', vm.newRestaurant);
        });

        $timeout(function () {
          var element = angular.element(document.querySelector('#locationInput'));
          var scope = element.scope();
          var $compile = element.injector().get('$compile');
          $compile(element)(scope);
        }, 0);
      };

      $scope.deleteRestaurant = function () {
        swal({
          title: "",
          text: "ARE YOU SURE YOU WANT TO DELETE <br> RESTAURANT A?",
          imageUrl: null,
          customClass: "app-alert no-title no-fieldset",
          showCancelButton: true,
          confirmButtonColor: "rgba(253,105,96,1)",
          confirmButtonText: "DELETE",
          cancelButtonText: "CANCEL",
          allowOutsideClick: true,
          html: true
        }, function () {
          console.log('click delete restaurant');
        });
      };
    }
  }

})();

(function () {
  'use strict';

  angular
    .module('app.restaurant')
    .config(restaurantConfig);

  restaurantConfig.$inject = ['$controllerProvider', '$compileProvider', '$filterProvider', '$provide', '$animateProvider'];
  function restaurantConfig($controllerProvider, $compileProvider, $filterProvider, $provide, $animateProvider) {

    var restaurant = angular.module('app.restaurant');
    // registering components after bootstrap
    restaurant.controller = $controllerProvider.register;
    restaurant.directive = $compileProvider.directive;
    restaurant.filter = $filterProvider.register;
    restaurant.factory = $provide.factory;
    restaurant.service = $provide.service;
    restaurant.constant = $provide.constant;
    restaurant.value = $provide.value;
    // Disables animation on items with class .ng-no-animation

    $animateProvider.classNameFilter(/^((?!(ng-no-animation)).)*$/);

  }

})();
/**=========================================================
 * Component: business-list.controller.js
 *
 =========================================================*/

(function () {
  'use strict';

  angular
    .module('app.restaurant')
    .controller('RestaurantCtrl', RestaurantCtrl)

  RestaurantCtrl.$inject = [
    '$rootScope'
    , '$scope'
    , '$state'
  ];

  function RestaurantCtrl($rootScope, $scope, $state) {
    var vm = this;

    vm.updatePlaceData = updatePlaceData
    /*
     GOOGLE PLACES scope
     auto complete
     */
    vm.ac_search = '' //searchfield
    vm.ac_details = null // google place data will be here
    vm.ac_options = { // options to obtain place details
      type: 'geocode',
      types: 'establishment',
      // bounds: {SWLat: 49, SWLng: -97, NELat: 50, NELng: -96},
      country: 'ca',
      language: 'eng',
      typesEnabled: false,
      boundsEnabled: false,
      componentEnabled: false,
      watchEnter: true
    };
    $scope.$watch('vm.ac_details', function (data) {
      if (data) {
        vm.contact.place_data = {}
        console.log('place details: ', data)
        var addr = {
          line_1: "",
          line_2: "",
          city: "",
          region: "",
          country: "",
          postal_code: "",
          place_id: data.place_id
        }
        _.each(data.address_components, function (el, key, list) {
          if (el.types) {
            if (el.types.indexOf("route") != -1) addr.line_1 = addr.line_1 + " " + el.long_name
            if (el.types.indexOf("street_number") != -1) addr.line_1 = el.long_name + addr.line_1
            if (el.types.indexOf("administrative_area_level_1") != -1) addr.region += el.long_name
            if (el.types.indexOf("country") != -1) addr.country += el.long_name
            if (el.types.indexOf("postal_code") != -1) addr.postal_code += el.long_name
            // sometimes city comes as sublocality and sometimes as locality
            if (el.types.indexOf("sublocality") != -1 && el.types.indexOf("sublocality_level_1") != -1) addr.city = el.long_name
            if (el.types.indexOf("locality") != -1 && el.types.indexOf("political") != -1) addr.city = el.long_name
          }
        });
        vm.contact.place_data.address = addr
        vm.updatePlaceData(data)
      }
    })


    function updatePlaceData(data) {
      console.log('updatePlaceData: ')
      if (data) {
        var merge_data = {
          opening_hours: _.omit(data.opening_hours, 'open_now')
          , place_id: data.place_id
          , website: data.website
          , name: data.name
          , phone: data.formatted_phone_number
          , formatted_address: data.formatted_address
          , international_phone_number: data.international_phone_number
        }

        if (data.geometry && data.geometry.location) {
          if (data.geometry.location.lat) merge_data.lat = data.geometry.location.lat()
          if (data.geometry.location.lng) merge_data.lng = data.geometry.location.lng()
        }
        vm.contact.place_data = _.extend(vm.contact.place_data, merge_data);
      }
    }

    activate();

    ////////////////

    function activate() {

      vm.contact = {};
      vm.okMsg = '';
      vm.errMsg = '';
      vm.edit = {
        contact: {
          location: false,
          email: false,
          phone: false,
          description: false
        }
      };

      vm.updateContactLocation = function () {
        vm.okMsg = '';
        vm.errMsg = '';
        if (vm.locationForm.$valid) {


          // vm.errMsg = 'Server request error';
          // vm.okMsg = 'Contact email have been changed';
          vm.edit.contact.location = false;
        } else {
          vm.locationForm.location.$dirty = true;
        }
      };
      
      vm.updateContactEmail = function () {
        vm.okMsg = '';
        vm.errMsg = '';
        if (vm.emailForm.$valid) {


          // vm.errMsg = 'Server request error';
          // vm.okMsg = 'Contact email have been changed';
          vm.edit.contact.email = false;
        } else {
          vm.emailForm.email.$dirty = true;
          vm.emailForm.confirm_email.$dirty = true;
        }
      };

      vm.updateContactPhone = function () {
        vm.okMsg = '';
        vm.errMsg = '';
        if (vm.phoneForm.$valid) {


          // vm.errMsg = 'Server request error';
          // vm.okMsg = 'Contact email have been changed';
          vm.edit.contact.phone = false;
        } else {
          vm.phoneForm.phonenumber.$dirty = true;
          vm.phoneForm.confirm_phonenumber.$dirty = true;
        }
      };

      vm.updateContactDescription = function () {
        vm.okMsg = '';
        vm.errMsg = '';
        vm.edit.contact.description = false;
        // vm.errMsg = 'Server request error';
        // vm.okMsg = 'Contact email have been changed';
        vm.edit.contact.description = false;
      };
      
      $rootScope.app.layout.hasSubTopNavBar = true;
      console.log('hasSubTopBar', $rootScope.app.layout.hasSubTopNavBar);
      $rootScope.tabId = 0;
      $rootScope.subtopnavbar = {
        tabId: 0,
        navBack: {
          uri: 'app.restaurant-overview',
          title: 'Restaurant overview'
        },
        items: [
          {title: 'Contact', iconClass: 'icon-profile'},
          {title: 'Access', iconClass: 'icon-manage-apps'},
        ]
      };

      $scope.addCollaborator = function () {
        var htmlText =
          "<div class='form-wrapper'>" +
          "<div class='form-item'>" +
          "<input type='text' ng-model='' placeholder='Name of collaborator'>" +
          "</div>" +
          "<div class='form-item'>" +
          "<span class='text-center'>-or-</span>" +
          "</div>" +
          "<div class='form-item'>" +
          "<input type='email' ng-model='' placeholder='Mail address'>" +
          "</div>" +
          "</div>";

        swal({
          title: "ADD NEW COLLABORATOR",
          text: htmlText,
          imageUrl: null,
          customClass: "app-alert has-close-button form-alert no-fieldset",
          showCancelButton: true,
          confirmButtonColor: "#5bc0de",
          confirmButtonText: "ADD",
          cancelButtonText: "",
          allowOutsideClick: true,
          html: true
        }, function () {
          console.log('click add collaborator');
        });
      };
    }
  }

})();

(function () {
  'use strict';

  angular
    .module('app.restaurant')
    .run(restaurantRun);

  restaurantRun.$inject = ['$rootScope', '$state', '$stateParams', '$window', '$templateCache', 'Colors', 'AuthService', '$localStorage', '$http'];

  function restaurantRun($rootScope, $state, $stateParams, $window, $templateCache, Colors, AuthService, $localStorage, $http) {

  }

})();


(function () {
  'use strict';

  angular
    .module('app.restaurant')
    .service('RestaurantService', RestaurantService);

  RestaurantService.$inject = ['$http', '$q', '$localStorage', '$httpParamSerializer', '$rootScope'];

  function RestaurantService($http, $q, $localStorage, $httpParamSerializer, $rootScope) {
  }

})();