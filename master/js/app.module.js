/*!
 * 
 * Angle - Bootstrap Admin App + AngularJS
 * 
 * Noocleus Angle Theme Seed: 0.1
 * Author: v.nikopolidi@gmaill.com * 
 * License: https://wrapbootstrap.com/help/licenses
 * 
 */

// APP START
// ----------------------------------- 

'use strict';
var apiBaseUrl = '';
var prefix = "/api/v1";
// gulp:overrideApiBaseUrl 
apiBaseUrl = 'http://demo-api.menucloud.io';
// gulp:endOverrideApiBaseUrl
var apiEndPoint = apiBaseUrl + prefix;
io.sails.url = apiBaseUrl;
(function () {
  angular
    .module('menucloud', [
      // global bower modules
      'ngSails'
      , 'ngAutodisable'
      , 'dialogs.main'
      , 'ui.bootstrap'
      , 'ui.select'
      , 'angular-momentjs'
      // ,'toaster'
      // ,'ui.ace'
      /*
       // has to get values from this provider and set it manually at app.translate
       ,'dialogs.default-translations'
       */
      // app components
      , 'app.core'
      , 'app.auth'
      , 'app.routes'
      , 'app.factories'
      , 'app.loadingbar'
      , 'app.navsearch'
      , 'app.preloader'
      , 'app.translate'
      , 'app.pages'
      , 'app.directives'
      , 'app.panels'
      , 'app.material'
      , 'app.settings'
      , 'app.sidebar'
      , 'app.utils'
      , 'app.material'
      , 'app.toaster'
      , 'app.elements'
      , 'app.dashboard'
      , 'app.restaurant'
      , 'app.subtopnavbar'
      , 'app.profile'
      // custom modules
      , 'menucloud.services'
      // other
    ]);
})();

