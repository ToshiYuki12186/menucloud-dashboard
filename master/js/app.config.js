(function() {
    angular
        .module('menucloud')
        .config(AppConfig);

    AppConfig.$inject = ['$sailsProvider','dialogsProvider'];
    function AppConfig($sailsProvider,dialogsProvider){
        /*
        ngSails
        */
        // console.log('apiBaseUrl prefix: ', apiBaseUrl, prefix)
        $sailsProvider.url = apiBaseUrl
        $sailsProvider.urlPrefix = prefix
        // console.log("$sailsProvider.url: ", $sailsProvider.url )
        dialogsProvider.useBackdrop('static');
        dialogsProvider.useFontAwesome()
        // dialogsProvider.useEscClose(false);
        // dialogsProvider.useCopy(false);
        dialogsProvider.setSize('sm');
    };

})();