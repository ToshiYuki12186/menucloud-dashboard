/**=========================================================
 * Component: business-list.controller.js
 *
 =========================================================*/

(function () {
  'use strict';

  angular
    .module('app.subtopnavbar')
    .controller('SubtopnavbarCtrl', SubtopnavbarCtrl)

  SubtopnavbarCtrl.$inject = [
    '$rootScope'
    , '$scope'
    , '$state'
  ];

  function SubtopnavbarCtrl($rootScope, $scope, $state) {
    var vm = this;

    activate();

    ////////////////

    function activate() {
      $rootScope.topnavbar.title = 'FIRST RESTAURANT';
      $rootScope.subtopnavbar.tabId = 0;
      $rootScope.subtopnavbar.navBack = {
        uri: '#'
      };

      $scope.onSelectTab = function (idx) {
        $rootScope.subtopnavbar.tabId = idx;
        console.log('selected tab idx: ', $rootScope.subtopnavbar.tabId);
      }
    }
  }

})();
