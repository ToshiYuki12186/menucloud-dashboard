(function () {
  'use strict';

  angular
    .module('app.profile')
    .config(profileConfig);

  profileConfig.$inject = ['$controllerProvider', '$compileProvider', '$filterProvider', '$provide', '$animateProvider'];
  function profileConfig($controllerProvider, $compileProvider, $filterProvider, $provide, $animateProvider) {

    var profile = angular.module('app.restaurant');
    // registering components after bootstrap
    profile.controller = $controllerProvider.register;
    profile.directive = $compileProvider.directive;
    profile.filter = $filterProvider.register;
    profile.factory = $provide.factory;
    profile.service = $provide.service;
    profile.constant = $provide.constant;
    profile.value = $provide.value;
    // Disables animation on items with class .ng-no-animation

    $animateProvider.classNameFilter(/^((?!(ng-no-animation)).)*$/);

  }

})();