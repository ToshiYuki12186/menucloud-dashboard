/**=========================================================
 * Component: business-list.controller.js
 *
 =========================================================*/

(function () {
  'use strict';

  angular
    .module('app.profile')
    .controller('ProfileCtrl', ProfileCtrl)

  ProfileCtrl.$inject = [
    '$rootScope'
    , '$scope'
    , '$state'
    , 'AuthService'
  ];

  function ProfileCtrl($rootScope, $scope, $state, AuthService) {
    var vm = this;

    activate();

    ////////////////
    function modifyBody(data) {
      var obj = {
        password: data.password,
        new_password: data.new_password
      }
      return obj;
    }

    function activate() {

      vm.account = {};
      vm.okMsg = '';
      vm.errMsg = '';

      vm.changePassword = function () {
        vm.okMsg = '';
        vm.errMsg = '';

        if (vm.passwordForm.$valid) {
          var body = modifyBody(vm.account);
          console.log('Body: ', body);
          AuthService.changePassword(body).then(function (data) {
            vm.okMsg = 'Your password has been updated!';
            $rootScope.profile.password.edit = false;
          }, function (err) {
            if (err) {
              if (err.message) {
                vm.errMsg = err.message
              } else {
                if (_.isString(err)) {
                  vm.errMsg = err
                } else {
                  vm.errMsg = "Unable to Change Password"
                }
              }
            } else {
              vm.errMsg = "Server Request Error"
            }
          });
        } else {
          vm.passwordForm.password.$dirty = true;
          vm.passwordForm.new_password.$dirty = true;
          vm.passwordForm.confirm_password.$dirty = true;
        }
      };
      
      $rootScope.topnavbar.title = 'SETTINGS';
      $rootScope.app.layout.hasSubTopNavBar = true;
      console.log('hasSubTopBar', $rootScope.app.layout.hasSubTopNavBar);
      $rootScope.tabId = 0;
      $rootScope.subtopnavbar = {
        tabId: 0,
        navBack: {
          uri: 'app.dashboard',
          title: 'Dashboard'
        },
        items: [
          {title: 'Profile', iconClass: 'icon-profile'},
          {title: 'Manage Apps', iconClass: 'icon-manage-apps'},
          {title: 'Billings', iconClass: 'icon-billing'}
        ]
      };

      $rootScope.billingInfo = {
        fields: [
          {id: 0, name: 'Date'},
          {id: 1, name: 'Payment'},
          {id: 2, name: 'App'},
          {id: 3, name: 'View'}
        ],
        data: [
          {
            date: 'June 1 / 2016',
            payment: '$20',
            app: 'menucloud'
          },{
            date: 'June 1 / 2016',
            payment: '$20',
            app: 'menucloud'
          }
        ]
      };

      $rootScope.myAppsInfo = [
        {
          selectedTab: 0,
          buttonText: 'UPGRADE',
          description: {
            logo: {
              img: 'app/img/logo5.png',
              title: ''
            },
            role: 'FREE',
            text: 'menuCLOUD is your all in one easy to use menu manager and publisher. Managae, design and publish your menus all from within one cloud based app.'
          },
          detail: [
            {
              id: 0,
              css: 'right-space',
              name: 'Your subscription',
              data: [
                {id: 0, val: 'Free'},
                {id: 1, val: '$0/month'}
              ]
            },{
              id: 0,
              css: 'has-separate',
              name: 'Added restaurants',
              data: [
                {id: 0, val: 'Restaurant one'},
                {id: 1, val: 'Restaurant two'},
                {id: 2, val: 'Restaurant three'}
              ]
            }
          ]
        },{
          selectedTab: 0,
          buttonText: 'LEARN MORE',
          description: {
            logo: {
              img: '',
              title: 'EVENTS'
            },
            role: 'COMING SOON',
            text: 'Manage your catering, guestlist and booking within one easy to use app. Easily capture and organize all your bookings for all your locations, and integrate with ease.'
          },
          detail: [
            {
              id: 0,
              css: 'right-space',
              name: 'Your subscription',
              data: [
                {id: 0, val: 'Free'},
                {id: 1, val: '$0/month'}
              ]
            },{
              id: 0,
              css: 'has-separate',
              name: 'Added restaurants',
              data: [
                {id: 0, val: 'Restaurant one'},
                {id: 1, val: 'Restaurant two'},
                {id: 2, val: 'Restaurant three'}
              ]
            }
          ]
        }
      ];

      $scope.editBillingInfo = function () {
        var htmlText =
          "<div class='form-wrapper'>" +
          "<div class='form-item'>" +
          "<input type='text' ng-model='' placeholder='Cardholders name'>" +
          "</div>" +
          "<div class='form-item'>" +
          "<input type='number' ng-model='' placeholder='Card number'>" +
          "</div>" +
          "<div class='form-item col-50 float-left icon-right icon-calendar1'>" +
          "<input type='number' ng-model='' placeholder='Year'>" +
          "</div>" +
          "<div class='form-item col-50 float-right icon-right icon-calendar1'>" +
          "<input type='number' ng-model='' placeholder='Month '>" +
          "</div>" +
          "<div class='float-clear'></div>"+
          "<div class='form-item col-50 float-left icon-right icon-calendar1'>" +
          "<input type='number' ng-model='' placeholder='CVV'>" +
          "</div>" +
          "<div class='float-clear'></div>"+
          "</div>";

        swal({
          title: "PAYMENT DETAILS",
          text: htmlText,
          imageUrl: null,
          customClass: "app-alert has-close-button form-alert no-fieldset",
          showCancelButton: true,
          confirmButtonColor: "#5bc0de",
          confirmButtonText: "Edit",
          cancelButtonText: "",
          allowOutsideClick: true,
          html: true
        }, function () {
          console.log('click edit billing info');
        });
      };
    }
  }

})();
