(function() {
    'use strict';

    angular
        .module('menucloud.services')
        .service('MenuService', MenuService);

    MenuService.$inject = [
      '$http'
      ,'$q'
      ,'$localStorage'
      ,'$httpParamSerializer'
      ,'$rootScope'
      ,'RestFactory'
      ,'$sails'
      /*,'MenuService'*/
    ];
    
    function MenuService($http, $q, $localStorage, $httpParamSerializer, $rootScope, RestFactory, $sails/*, MenuService*/) {

        
        var model_name = 'menu'
        var Service = function(){
          RestFactory.apply(this,arguments)
        }
        var Serviceinstance = new Service(model_name)
        
        /*********************************************************************************
        @GUIDE:
        */
        // custom method example (see ../components/factories/rest.factory for details)
        /*
        Serviceinstance.yourAction = yourAction

        function yourAction(id, body){
          var action = '/action/'+id
          
          // arguments: (method, action, body)
          // @method: 'get' || 'put' || 'post' || 'delete'
          // @action: '/someaction'
          // @body: Object @optional
          
          return Serviceinstance.customAction('put', action, body)
        }
        **********************************************************************************/
        return  Serviceinstance;

        /*
        // @NOTE: commented out cause I'm not sure if we will need these methods here
        Service.prototype.removeSource = function (id,file_id) {
          var that = this
          var deferred = $q.defer();
          var path = that.endpoint + '/'+id+'/sources/'+file_id
          var promise
          if(that.sails_on){
            promise = $sails.delete(path).then(deferred.resolve, deferred.reject)
          }else{
            promise = $http.delete(path).then(deferred.resolve, deferred.reject)
          }
          return deferred.promise;
        }

        Service.prototype.completeMenu = completeMenu
        function completeMenu(id){
          var that = this
          var deferred = $q.defer();
          var path = that.endpoint + '/'+id+'/completeMenu'
          var promise
          if(that.sails_on){
            promise = $sails.put(path).then(deferred.resolve, deferred.reject)
          }else{
            promise = $http.put(path).then(deferred.resolve, deferred.reject)
          }
          return deferred.promise;
        }
        Service.prototype.updateItems = updateItems
        
        function updateItems(body){
          var that = this
          var deferred = $q.defer();
          var path = that.endpoint + '/'+body.id+'/items'
          var promise
          if(that.sails_on){
            promise = $sails.put(path, body).then(deferred.resolve, deferred.reject)
          }else{
            promise = $http.put(path, body).then(deferred.resolve, deferred.reject)
          }
          return deferred.promise;
        }
        
        Service.prototype.populateItems = populateItems
        function populateItems(id){
          var that = this
          var deferred = $q.defer();
          var path = that.endpoint + '/'+id+'/items'
          var promise
          if(that.sails_on){
            promise = $sails.get(path).then(deferred.resolve, deferred.reject)
          }else{
            promise = $http.get(path).then(deferred.resolve, deferred.reject)
          }
          return deferred.promise;
        }
        
        */
    }
})();