/**=========================================================
 * Component: business-list.controller.js
 *
 =========================================================*/

(function () {
  'use strict';

  angular
    .module('app.dashboard')
    .controller('DashboardCtrl', DashboardCtrl)

  DashboardCtrl.$inject = [
    '$rootScope'
    , '$scope'
    , '$state'
  ];

  function DashboardCtrl($rootScope, $scope, $state) {
    var vm = this;

    activate();

    ////////////////

    function activate() {
      $rootScope.topnavbar.title = 'MENU OVERVIEW';
      $rootScope.app.layout.hasSubTopNavBar = false;
    }
  }

})();
