(function () {
  'use strict';

  angular
    .module('app.dashboard')
    .config(dashboardConfig);

  dashboardConfig.$inject = ['$controllerProvider', '$compileProvider', '$filterProvider', '$provide', '$animateProvider'];
  function dashboardConfig($controllerProvider, $compileProvider, $filterProvider, $provide, $animateProvider) {

    var dashboard = angular.module('app.dashboard');
    // registering components after bootstrap
    dashboard.controller = $controllerProvider.register;
    dashboard.directive = $compileProvider.directive;
    dashboard.filter = $filterProvider.register;
    dashboard.factory = $provide.factory;
    dashboard.service = $provide.service;
    dashboard.constant = $provide.constant;
    dashboard.value = $provide.value;
    // Disables animation on items with class .ng-no-animation

    $animateProvider.classNameFilter(/^((?!(ng-no-animation)).)*$/);

  }

})();