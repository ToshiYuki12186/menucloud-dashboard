/**=========================================================
 * Component: business-list.controller.js
 *
 =========================================================*/

(function () {
  'use strict';

  angular
    .module('app.restaurant')
    .controller('RestaurantCtrl', RestaurantCtrl)

  RestaurantCtrl.$inject = [
    '$rootScope'
    , '$scope'
    , '$state'
  ];

  function RestaurantCtrl($rootScope, $scope, $state) {
    var vm = this;

    vm.updatePlaceData = updatePlaceData
    /*
     GOOGLE PLACES scope
     auto complete
     */
    vm.ac_search = '' //searchfield
    vm.ac_details = null // google place data will be here
    vm.ac_options = { // options to obtain place details
      type: 'geocode',
      types: 'establishment',
      // bounds: {SWLat: 49, SWLng: -97, NELat: 50, NELng: -96},
      country: 'ca',
      language: 'eng',
      typesEnabled: false,
      boundsEnabled: false,
      componentEnabled: false,
      watchEnter: true
    };
    $scope.$watch('vm.ac_details', function (data) {
      if (data) {
        vm.contact.place_data = {}
        console.log('place details: ', data)
        var addr = {
          line_1: "",
          line_2: "",
          city: "",
          region: "",
          country: "",
          postal_code: "",
          place_id: data.place_id
        }
        _.each(data.address_components, function (el, key, list) {
          if (el.types) {
            if (el.types.indexOf("route") != -1) addr.line_1 = addr.line_1 + " " + el.long_name
            if (el.types.indexOf("street_number") != -1) addr.line_1 = el.long_name + addr.line_1
            if (el.types.indexOf("administrative_area_level_1") != -1) addr.region += el.long_name
            if (el.types.indexOf("country") != -1) addr.country += el.long_name
            if (el.types.indexOf("postal_code") != -1) addr.postal_code += el.long_name
            // sometimes city comes as sublocality and sometimes as locality
            if (el.types.indexOf("sublocality") != -1 && el.types.indexOf("sublocality_level_1") != -1) addr.city = el.long_name
            if (el.types.indexOf("locality") != -1 && el.types.indexOf("political") != -1) addr.city = el.long_name
          }
        });
        vm.contact.place_data.address = addr
        vm.updatePlaceData(data)
      }
    })


    function updatePlaceData(data) {
      console.log('updatePlaceData: ')
      if (data) {
        var merge_data = {
          opening_hours: _.omit(data.opening_hours, 'open_now')
          , place_id: data.place_id
          , website: data.website
          , name: data.name
          , phone: data.formatted_phone_number
          , formatted_address: data.formatted_address
          , international_phone_number: data.international_phone_number
        }

        if (data.geometry && data.geometry.location) {
          if (data.geometry.location.lat) merge_data.lat = data.geometry.location.lat()
          if (data.geometry.location.lng) merge_data.lng = data.geometry.location.lng()
        }
        vm.contact.place_data = _.extend(vm.contact.place_data, merge_data);
      }
    }

    activate();

    ////////////////

    function activate() {

      vm.contact = {};
      vm.okMsg = '';
      vm.errMsg = '';
      vm.edit = {
        contact: {
          location: false,
          email: false,
          phone: false,
          description: false
        }
      };

      vm.updateContactLocation = function () {
        vm.okMsg = '';
        vm.errMsg = '';
        if (vm.locationForm.$valid) {


          // vm.errMsg = 'Server request error';
          // vm.okMsg = 'Contact email have been changed';
          vm.edit.contact.location = false;
        } else {
          vm.locationForm.location.$dirty = true;
        }
      };
      
      vm.updateContactEmail = function () {
        vm.okMsg = '';
        vm.errMsg = '';
        if (vm.emailForm.$valid) {


          // vm.errMsg = 'Server request error';
          // vm.okMsg = 'Contact email have been changed';
          vm.edit.contact.email = false;
        } else {
          vm.emailForm.email.$dirty = true;
          vm.emailForm.confirm_email.$dirty = true;
        }
      };

      vm.updateContactPhone = function () {
        vm.okMsg = '';
        vm.errMsg = '';
        if (vm.phoneForm.$valid) {


          // vm.errMsg = 'Server request error';
          // vm.okMsg = 'Contact email have been changed';
          vm.edit.contact.phone = false;
        } else {
          vm.phoneForm.phonenumber.$dirty = true;
          vm.phoneForm.confirm_phonenumber.$dirty = true;
        }
      };

      vm.updateContactDescription = function () {
        vm.okMsg = '';
        vm.errMsg = '';
        vm.edit.contact.description = false;
        // vm.errMsg = 'Server request error';
        // vm.okMsg = 'Contact email have been changed';
        vm.edit.contact.description = false;
      };
      
      $rootScope.app.layout.hasSubTopNavBar = true;
      console.log('hasSubTopBar', $rootScope.app.layout.hasSubTopNavBar);
      $rootScope.tabId = 0;
      $rootScope.subtopnavbar = {
        tabId: 0,
        navBack: {
          uri: 'app.restaurant-overview',
          title: 'Restaurant overview'
        },
        items: [
          {title: 'Contact', iconClass: 'icon-profile'},
          {title: 'Access', iconClass: 'icon-manage-apps'},
        ]
      };

      $scope.addCollaborator = function () {
        var htmlText =
          "<div class='form-wrapper'>" +
          "<div class='form-item'>" +
          "<input type='text' ng-model='' placeholder='Name of collaborator'>" +
          "</div>" +
          "<div class='form-item'>" +
          "<span class='text-center'>-or-</span>" +
          "</div>" +
          "<div class='form-item'>" +
          "<input type='email' ng-model='' placeholder='Mail address'>" +
          "</div>" +
          "</div>";

        swal({
          title: "ADD NEW COLLABORATOR",
          text: htmlText,
          imageUrl: null,
          customClass: "app-alert has-close-button form-alert no-fieldset",
          showCancelButton: true,
          confirmButtonColor: "#5bc0de",
          confirmButtonText: "ADD",
          cancelButtonText: "",
          allowOutsideClick: true,
          html: true
        }, function () {
          console.log('click add collaborator');
        });
      };
    }
  }

})();
