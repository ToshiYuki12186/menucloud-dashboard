(function () {
  'use strict';

  angular
    .module('app.restaurant')
    .config(restaurantConfig);

  restaurantConfig.$inject = ['$controllerProvider', '$compileProvider', '$filterProvider', '$provide', '$animateProvider'];
  function restaurantConfig($controllerProvider, $compileProvider, $filterProvider, $provide, $animateProvider) {

    var restaurant = angular.module('app.restaurant');
    // registering components after bootstrap
    restaurant.controller = $controllerProvider.register;
    restaurant.directive = $compileProvider.directive;
    restaurant.filter = $filterProvider.register;
    restaurant.factory = $provide.factory;
    restaurant.service = $provide.service;
    restaurant.constant = $provide.constant;
    restaurant.value = $provide.value;
    // Disables animation on items with class .ng-no-animation

    $animateProvider.classNameFilter(/^((?!(ng-no-animation)).)*$/);

  }

})();